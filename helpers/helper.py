import time
import datetime
import jwt
import json
import csv
import requests
from passlib.hash import pbkdf2_sha256
from settings import configuration
from flask import Response
from helpers.jsonencoder import JSONEncoder
from reportlab.lib.units import cm, inch
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import *
from reportlab.lib.styles import getSampleStyleSheet
from flask import request
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile



class Helper(object):

    def date_to_timestamp(self, date: str):

        timestamp = time.mktime(datetime.datetime.strptime(
            date, "%Y-%m-%dT%H:%M:%S%z").timetuple())

        return int(timestamp)

    @staticmethod
    def generate_hash(password):
        return pbkdf2_sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return pbkdf2_sha256.verify(password, hash)

    @staticmethod
    def jwt_encode(payload: dict):
        payload["iat"] = datetime.datetime.utcnow()
        payload["exp"] = datetime.datetime.utcnow() + \
            datetime.timedelta(days=1)
        return jwt.encode(payload, configuration.jwt_secret_key, algorithm=configuration.jwt_algorithm).decode("utf-8")

    @staticmethod
    def convert_csv(filename, data, metadata):
        try:
            with open(filename, 'w', newline='') as f:
                writer = csv.DictWriter(f, fieldnames=metadata)
                writer.writeheader()
                writer.writerows(data)
        except Exception as e:
            print("I/O error", e) 
        
        return(filename)

    @staticmethod
    def convert_xls(filename, data, metadata):  
        try:
            df = pd.DataFrame(data)  
            writer = ExcelWriter(filename)
            df.to_excel(writer,'data',index=False)
            writer.save()
        except Exception as e:
            print("I/O error", e) 
        
        return(filename)

    @staticmethod
    def convert_pdf(filename):
        print('masuk')
        # Data from CSV
        with open(filename, "r") as csvfile:
            data = list(csv.reader(csvfile))
            print(data)

        elements = []

        # PDF Text
        # PDF Text - Styles
        styles = getSampleStyleSheet()
        styleNormal = styles['Normal']

        # PDF Text - Content
        line1 = 'Data ' + filename
        line2 = 'Tanggal Cetak {}'.format(datetime.datetime.now().strftime("%d-%m-%Y"))
        line3 = 'Diunduh dari http://satudata.103.122.5.84.xip.io '
        elements.append(Paragraph(line1, styleNormal))
        elements.append(Paragraph(line2, styleNormal))
        elements.append(Paragraph(line3, styleNormal))
        elements.append(Spacer(inch, .25 * inch))

        # PDF Table
        # PDF Table - Styles
        table_style = TableStyle([
            ('VALIGN', (0, 0), (-1, -1), 'TOP'), 
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'), 
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ])

        # Add table to elements
        t = Table(data, style=table_style, hAlign='LEFT')
        elements.append(t)

        # Generate PDF
        archivo_pdf = SimpleDocTemplate(
            filename[:-3]+'pdf',
            pagesize=letter,
            rightMargin=40,
            leftMargin=40,
            topMargin=40,
            bottomMargin=28,)
        archivo_pdf.build(elements)
        return(filename[:-3]+'pdf')

    
    @staticmethod
    def read_jwt():
        try:
            token = request.headers.get('Authorization', None)
            tokens = token.split(" ")
            payload = jwt.decode(tokens[1], configuration.jwt_secret_key, algorithm=[configuration.jwt_algorithm])
            return payload
        except Exception as e:
            return {}
        
    @staticmethod
    def decode_jwt(token):
        try:
            token = token.split(" ")
            payload = jwt.decode(token[1], configuration.jwt_secret_key, algorithm=[configuration.jwt_algorithm])
            return payload
        except Exception as e:
            return {}