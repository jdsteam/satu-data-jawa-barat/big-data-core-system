from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisbunOpendataLuasTanamanSemusimPerkebunanRakyatModel(BaseModel, db.Model):
    """Model for the disbun_opendata_luas_tanaman_semusim_perkebunan_rakyat table"""
    __tablename__ = 'opendata_luas_tanaman_semusim_perkebunan_rakyat'
    __table_args__ = {'schema': 'disbun'}

    # start model
    provinsi = db.Column(db.String(255))
    kabupaten_atau_kota = db.Column(db.String(255))
    kode_kabupaten_atau_kota = db.Column(db.Integer)
    kategori_kegiatan = db.Column(db.String(255))
    jenis_komoditi = db.Column(db.String(255))
    luas_tanaman = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
