from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class Bp2DOpendataCreativeResearchJournalVolume4Bp2DJawaBaratModel(BaseModel, db.Model):
    """Model for the bp2d_opendata_creative_research_journal_volume_4_bp2d_jawa_barat table"""
    __tablename__ = 'opendata_creative_research_journal_volume_4_bp2d_jawa_barat'
    __table_args__ = {'schema': 'bp2d'}

    # start model
    peneliti = db.Column(db.String(255))
    judul_penelitian = db.Column(db.String(255))
    kata_kunci = db.Column(db.String(255))
    volume = db.Column(db.Integer)
    nomor = db.Column(db.Integer)
    bulan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    halaman = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
