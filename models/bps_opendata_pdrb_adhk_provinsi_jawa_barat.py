from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpsOpendataPdrbAdhkProvinsiJawaBaratModel(BaseModel, db.Model):
    """Model for the bps_opendata_pdrb_adhk_provinsi_jawa_barat table"""
    __tablename__ = 'opendata_pdrb_adhk_provinsi_jawa_barat'
    __table_args__ = {'schema': 'bps'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    pdrb_adhk = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
