from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskesOpendataJumlahTpmBerdasarkanStatusHigieneSanitasiModel(BaseModel, db.Model):
    """Model for the diskes_opendata_jumlah_tpm_berdasarkan_status_higiene_sanitasi table"""
    __tablename__ = 'opendata_jumlah_tpm_berdasarkan_status_higiene_sanitasi'
    __table_args__ = {'schema': 'diskes'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    status_tpm_higiene_sanitasi = db.Column(db.String(255))
    sektor = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
