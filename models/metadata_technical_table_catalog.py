from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class MetadataTechnicalTableCatalogModel(BaseModel, db.Model):
    """Model for the metadata_technical_table_catalog table"""
    __tablename__ = 'technical_table_catalog'
    __table_args__ = {'schema': 'metadata'}

    # start model
    table_schema = db.Column(db.String)
    table_name = db.Column(db.String)
    column_name = db.Column(db.String)
    column_type = db.Column(db.String)
    constraint_name = db.Column(db.String)
    constraint_type = db.Column(db.String(255))
    foreign_table_schema = db.Column(db.String)
    foreign_table_name = db.Column(db.String)
    foreign_column_name = db.Column(db.String)
    id = db.Column(db.Integer, primary_key=True)
