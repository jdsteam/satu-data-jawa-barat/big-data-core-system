from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PsdaOpendataPanjangPantaiProvinsiJawaBaratModel(BaseModel, db.Model):
    """Model for the psda_opendata_panjang_pantai_provinsi_jawa_barat table"""
    __tablename__ = 'opendata_panjang_pantai_provinsi_jawa_barat'
    __table_args__ = {'schema': 'psda'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    pantai = db.Column(db.String(255))
    batas = db.Column(db.String(255))
    panjang_pantai = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
