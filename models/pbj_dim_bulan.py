from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimBulanModel(BaseModel, db.Model):
    """Model for the pbj_dim_bulan table"""
    __tablename__ = 'dim_bulan'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idtahun = db.Column(db.String(255), primary_key=True)
    tahun = db.Column(db.String(255))
