from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class GenerateModel(BaseModel, db.Model):
    """Model for the generate table"""
    __tablename__ = 'generate'
    __table_args__ = {'schema': 'public'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    endpoint = db.Column(db.String(255))
    opd = db.Column(db.String(255))
    table_name = db.Column(db.String(255))