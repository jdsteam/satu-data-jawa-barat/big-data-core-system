from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PsdaOpendataInventarisasiSungaiNonLintasPanjangProvinsiJawaModel(BaseModel, db.Model):
    """Model for the psda_opendata_inventarisasi_sungai_non_lintas_panjang_provinsi_jawa table"""
    __tablename__ = 'opendata_inventarisasi_sungai_non_lintas_panjang_provinsi_jawa'
    __table_args__ = {'schema': 'psda'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    panjang_sungai = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
