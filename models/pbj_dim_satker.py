from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimSatkerModel(BaseModel, db.Model):
    """Model for the pbj_dim_satker table"""
    __tablename__ = 'dim_satker'
    __table_args__ = {'schema': 'pbj'}

    # start model
    kd_satker = db.Column(db.Float)
    nama_lpse = db.Column(db.String(255))
    nama_satker = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
