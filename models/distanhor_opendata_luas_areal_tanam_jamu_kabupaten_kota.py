from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DistanhorOpendataLuasArealTanamJamuKabupatenKotaModel(BaseModel, db.Model):
    """Model for the distanhor_opendata_luas_areal_tanam_jamu_kabupaten_kota table"""
    __tablename__ = 'opendata_luas_areal_tanam_jamu_kabupaten_kota'
    __table_args__ = {'schema': 'distanhor'}

    # start model
    NA = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
