from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimSkpdModel(BaseModel, db.Model):
    """Model for the pbj_dim_skpd table"""
    __tablename__ = 'dim_skpd'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idskpd = db.Column(db.Integer)
    tahun = db.Column(db.Integer)
    iddaerah = db.Column(db.Integer)
    kodeunit = db.Column(db.String(255))
    kodeskpd = db.Column(db.String(255))
    namaskpd = db.Column(db.String(255))
    komisi = db.Column(db.String(255))
    namakepala = db.Column(db.String(255))
    nipkepala = db.Column(db.String(255))
    pangkatkepala = db.Column(db.String(255))
    namabendahara = db.Column(db.String(255))
    nipbendahara = db.Column(db.String(255))
    nodpa = db.Column(db.Integer)
    namaplh = db.Column(db.String(255))
    nipplh = db.Column(db.String(255))
    pangkatplh = db.Column(db.String(255))
    namaplt = db.Column(db.String(255))
    nipplt = db.Column(db.String(255))
    pangkatplt = db.Column(db.String(255))
    islocked = db.Column(db.Integer)
    createduser = db.Column(db.Integer)
    createdat = db.Column(db.String(255))
    createdip = db.Column(db.String(255))
    updateduser = db.Column(db.Integer)
    updatedat = db.Column(db.String(255))
    updatedip = db.Column(db.String(255))
    noskpd = db.Column(db.String(255))
    opdsipkdteks = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
