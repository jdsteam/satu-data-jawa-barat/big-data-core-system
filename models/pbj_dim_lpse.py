from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimLpseModel(BaseModel, db.Model):
    """Model for the pbj_dim_lpse table"""
    __tablename__ = 'dim_lpse'
    __table_args__ = {'schema': 'pbj'}

    # start model
    kd_lpse = db.Column(db.Float)
    nama_lpse = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
