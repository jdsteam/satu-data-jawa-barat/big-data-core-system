from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PsdaOpendataNamaDanLuasDasWsCimanukCisanggarungModel(BaseModel, db.Model):
    """Model for the psda_opendata_nama_dan_luas_das_ws_cimanuk_cisanggarung table"""
    __tablename__ = 'opendata_nama_dan_luas_das_ws_cimanuk_cisanggarung'
    __table_args__ = {'schema': 'psda'}

    # start model
    kode_provinsi = db.Column(db.Integer)
    provinsi = db.Column(db.String(255))
    nama_das = db.Column(db.String(255))
    luas_das = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
