from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskanlutOpendataJumlahProduksiBudidayaPembesaranProvinsiModel(BaseModel, db.Model):
    """Model for the diskanlut_opendata_jumlah_produksi_budidaya_pembesaran_provinsi table"""
    __tablename__ = 'opendata_jumlah_produksi_budidaya_pembesaran_provinsi'
    __table_args__ = {'schema': 'diskanlut'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    semester = db.Column(db.String(255))
    produksi = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
