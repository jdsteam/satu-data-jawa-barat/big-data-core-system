from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DpmdesOpendataIndeksDesaMembangunJawaBaratModel(BaseModel, db.Model):
    """Model for the dpmdes_opendata_indeks_desa_membangun_jawa_barat table"""
    __tablename__ = 'opendata_indeks_desa_membangun_jawa_barat'
    __table_args__ = {'schema': 'dpmdes'}

    # start model
    kategori = db.Column(db.String(255))
    indeks = db.Column(db.Integer)
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
