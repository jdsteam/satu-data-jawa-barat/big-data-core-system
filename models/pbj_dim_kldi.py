from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimKldiModel(BaseModel, db.Model):
    """Model for the pbj_dim_kldi table"""
    __tablename__ = 'dim_kldi'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idkldi = db.Column(db.String(255))
    namakldi = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
