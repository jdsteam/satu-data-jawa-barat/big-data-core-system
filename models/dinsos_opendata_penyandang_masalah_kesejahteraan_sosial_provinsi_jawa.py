from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DinsosOpendataPenyandangMasalahKesejahteraanSosialProvinsiJawaModel(BaseModel, db.Model):
    """Model for the dinsos_opendata_penyandang_masalah_kesejahteraan_sosial_provinsi_jawa table"""
    __tablename__ = 'opendata_penyandang_masalah_kesejahteraan_sosial_provinsi_jawa'
    __table_args__ = {'schema': 'dinsos'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    masalah_kesejahteraan_sosial = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
