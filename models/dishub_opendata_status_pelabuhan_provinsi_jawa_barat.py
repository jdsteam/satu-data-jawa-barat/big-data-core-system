from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DishubOpendataStatusPelabuhanProvinsiJawaBaratModel(BaseModel, db.Model):
    """Model for the dishub_opendata_status_pelabuhan_provinsi_jawa_barat table"""
    __tablename__ = 'opendata_status_pelabuhan_provinsi_jawa_barat'
    __table_args__ = {'schema': 'dishub'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    nama_pelabuhan = db.Column(db.String(255))
    status_pelabuhan = db.Column(db.String(255))
    status_hirarki = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
