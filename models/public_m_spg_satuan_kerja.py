from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PublicMSpgSatuanKerjaModel(BaseModel, db.Model):
    """Model for the public_m_spg_satuan_kerja table"""
    __tablename__ = 'm_spg_satuan_kerja'
    __table_args__ = {'schema': 'public'}

    # start model
    satuan_kerja_id = db.Column(db.Integer, primary_key=True)
    tahun_id = db.Column(db.Integer)
    satuan_kerja_nama = db.Column(db.String(255))
    kode = db.Column(db.String(15))
    satuan_kerja_alamat = db.Column(db.String(150))
    satuan_kerja_kel_ds = db.Column(db.String(50))
    kecamatan_id = db.Column(db.Integer)
    satuan_kerja_khusus = db.Column(db.String(1))
    status = db.Column(db.Integer)
    kode_skpd = db.Column(db.String(255))
    latitude = db.Column(db.REAL)
    longitude = db.Column(db.REAL)
    kota = db.Column(db.String(255))
    kecamatan = db.Column(db.String(255))
    kelurahan = db.Column(db.String(255))
