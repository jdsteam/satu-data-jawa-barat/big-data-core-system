from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpsdmOpendataJenisDanJumlahLulusanPengembanganKompetensiModel(BaseModel, db.Model):
    """Model for the bpsdm_opendata_jenis_dan_jumlah_lulusan_pengembangan_kompetensi table"""
    __tablename__ = 'opendata_jenis_dan_jumlah_lulusan_pengembangan_kompetensi'
    __table_args__ = {'schema': 'bpsdm'}

    # start model
    pengembangan_kompetensi = db.Column(db.String(255))
    jumlah_lulusan = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
