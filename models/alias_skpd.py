from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class AliasSkpdModel(BaseModel, db.Model):
    """Model for the alias_skpd table"""
    __tablename__ = 'skpd'
    __table_args__ = {'schema': 'alias'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    regional_id = db.Column(db.Integer, primary_key=True)
    kode_skpd = db.Column(db.String(255))
    nama_skpd = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True))
    cuid = db.Column(db.Integer)
    description = db.Column(db.String(255))
    address = db.Column(db.String(255))
    phone = db.Column(db.String(255))
    email = db.Column(db.String(255))
    media_website = db.Column(db.String(255))
    media_facebook = db.Column(db.String(255))
    media_twitter = db.Column(db.String(255))
    media_instagram = db.Column(db.String(255))
    media_youtube = db.Column(db.String(255))
    logo = db.Column(db.String(255))
    nama_skpd_alias = db.Column(db.String(255))
    is_external = db.Column(db.Boolean)
    count_dataset = db.Column(db.Integer)
