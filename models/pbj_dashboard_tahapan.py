from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardTahapanModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_tahapan table"""
    __tablename__ = 'dashboard_tahapan'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    namatahapan = db.Column(db.String(255))
    namatahapanspse = db.Column(db.String(255))
