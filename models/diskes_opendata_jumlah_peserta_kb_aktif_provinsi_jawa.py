from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskesOpendataJumlahPesertaKbAktifProvinsiJawaModel(BaseModel, db.Model):
    """Model for the diskes_opendata_jumlah_peserta_kb_aktif_provinsi_jawa table"""
    __tablename__ = 'opendata_jumlah_peserta_kb_aktif_provinsi_jawa'
    __table_args__ = {'schema': 'diskes'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    metode_kb = db.Column(db.String(255))
    jenis_kb = db.Column(db.String(255))
    jumlah = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
