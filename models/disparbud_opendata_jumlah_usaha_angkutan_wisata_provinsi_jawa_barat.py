from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisparbudOpendataJumlahUsahaAngkutanWisataProvinsiJawaBaratModel(BaseModel, db.Model):
    """Model for the disparbud_opendata_jumlah_usaha_angkutan_wisata_provinsi_jawa_barat table"""
    __tablename__ = 'opendata_jumlah_usaha_angkutan_wisata_provinsi_jawa_barat'
    __table_args__ = {'schema': 'disparbud'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    usaha_angkutan_wisata = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
