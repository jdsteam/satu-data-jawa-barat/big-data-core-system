from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisparbudOpendataTenagaKerjaRestoranRumahMakanCafeProvinsiJawaModel(BaseModel, db.Model):
    """Model for the disparbud_opendata_tenaga_kerja_restoran_rumah_makan_cafe_provinsi_jawa table"""
    __tablename__ = 'opendata_tenaga_kerja_restoran_rumah_makan_cafe_provinsi_jawa'
    __table_args__ = {'schema': 'disparbud'}

    # start model
    NA = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
