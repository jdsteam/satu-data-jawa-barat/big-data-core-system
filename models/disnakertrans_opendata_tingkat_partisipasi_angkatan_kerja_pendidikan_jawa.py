from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisnakertransOpendataTingkatPartisipasiAngkatanKerjaPendidikanJawaModel(BaseModel, db.Model):
    """Model for the disnakertrans_opendata_tingkat_partisipasi_angkatan_kerja_pendidikan_jawa table"""
    __tablename__ = 'opendata_tingkat_partisipasi_angkatan_kerja_pendidikan_jawa'
    __table_args__ = {'schema': 'disnakertrans'}

    # start model
    pendidikan = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
