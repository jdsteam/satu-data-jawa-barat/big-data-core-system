from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BapendaPotensipajakJumlahModel(BaseModel, db.Model):
    """Model for the bapenda_potensipajak_jumlah table"""
    __tablename__ = 'potensipajak_jumlah'
    __table_args__ = {'schema': 'bapenda'}

    # start model
    idbulan = db.Column(db.Integer)
    idlokasi = db.Column(db.Integer)
    persentase = db.Column(db.REAL)
    realisasi = db.Column(db.Float)
    tahun = db.Column(db.Integer)
    target = db.Column(db.Float)
    id = db.Column(db.Integer, primary_key=True)
