from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpsPotensiWisataKecamatanModel(BaseModel, db.Model):
    """Model for the bps_potensi_wisata_kecamatan table"""
    __tablename__ = 'potensi_wisata_kecamatan'
    __table_args__ = {'schema': 'bps'}

    # start model
    nama_provinsi = db.Column(db.String(255))
    kode_provinsi = db.Column(db.String(255))
    nama_kabupaten_kota = db.Column(db.String(255))
    kode_kabupaten_kode = db.Column(db.String(255))
    nama_kecamatan = db.Column(db.String(255))
    keberadaan_desa_wisata_yang_ditetapkan = db.Column(db.String(255))
    jumlah_desa_wisata = db.Column(db.String(255))
    keberadaan_wisata_tirta = db.Column(db.String(255))
    jumlah_wisata_tirta = db.Column(db.String(255))
    keberadaan_agrowisata = db.Column(db.String(255))
    jumlah_agrowisata = db.Column(db.String(255))
    keberadaan_wisata_budaya = db.Column(db.String(255))
    jumlah_wisata_budaya = db.Column(db.String(255))
    keberadaan_wisata_alam = db.Column(db.String(255))
    jumlah_wisata_alam = db.Column(db.String(255))
    keberadaan_daya_tarik_wisata_lainnya = db.Column(db.String(255))
    jumlah_daya_tarik_wisata_lainnya = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
