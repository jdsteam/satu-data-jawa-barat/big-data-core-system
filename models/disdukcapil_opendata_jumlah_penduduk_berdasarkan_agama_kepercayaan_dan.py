from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisdukcapilOpendataJumlahPendudukBerdasarkanAgamaKepercayaanDanModel(BaseModel, db.Model):
    """Model for the disdukcapil_opendata_jumlah_penduduk_berdasarkan_agama_kepercayaan_dan table"""
    __tablename__ = 'opendata_jumlah_penduduk_berdasarkan_agama_kepercayaan_dan'
    __table_args__ = {'schema': 'disdukcapil'}

    # start model
    agama_kepercayaan = db.Column(db.String(255))
    jenis_kelamin = db.Column(db.String(255))
    jumlah_penduduk = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
