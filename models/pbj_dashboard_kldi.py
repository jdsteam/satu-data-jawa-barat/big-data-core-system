from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardKldiModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_kldi table"""
    __tablename__ = 'dashboard_kldi'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.String(10), primary_key=True)
    namakldi = db.Column(db.String(255))
