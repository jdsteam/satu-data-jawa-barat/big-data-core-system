from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DtphKomoditasModel(BaseModel, db.Model):
    """Model for the dtph_komoditas table"""
    __tablename__ = 'komoditas'
    __table_args__ = {'schema': 'dtph'}

    # start model
    kodekomoditi = db.Column(db.Integer, primary_key=True)
    namakomoditi = db.Column(db.String)
    avgharga = db.Column(db.Float)
    unitharga = db.Column(db.String)
    updatedtime = db.Column(db.DateTime(True))
