from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisbunOpendataLuasTanamanTahunanPerkebunanRakyatModel(BaseModel, db.Model):
    """Model for the disbun_opendata_luas_tanaman_tahunan_perkebunan_rakyat table"""
    __tablename__ = 'opendata_luas_tanaman_tahunan_perkebunan_rakyat'
    __table_args__ = {'schema': 'disbun'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kabupaten_kota = db.Column(db.Integer)
    nama_kabupaten_kota = db.Column(db.String(255))
    jenis_komoditi = db.Column(db.String(255))
    kategori_kegiatan = db.Column(db.String(255))
    luas_tanaman = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
