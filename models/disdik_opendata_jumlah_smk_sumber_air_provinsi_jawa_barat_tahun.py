from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisdikOpendataJumlahSmkSumberAirProvinsiJawaBaratTahunModel(BaseModel, db.Model):
    """Model for the disdik_opendata_jumlah_smk_sumber_air_provinsi_jawa_barat_tahun table"""
    __tablename__ = 'opendata_jumlah_smk_sumber_air_provinsi_jawa_barat_tahun'
    __table_args__ = {'schema': 'disdik'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    sumber_air = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
