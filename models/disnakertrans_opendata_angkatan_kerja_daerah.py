from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisnakertransOpendataAngkatanKerjaDaerahModel(BaseModel, db.Model):
    """Model for the disnakertrans_opendata_angkatan_kerja_daerah table"""
    __tablename__ = 'opendata_angkatan_kerja_daerah'
    __table_args__ = {'schema': 'disnakertrans'}

    # start model
    daerah = db.Column(db.String(255))
    angkatan_kerja = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
