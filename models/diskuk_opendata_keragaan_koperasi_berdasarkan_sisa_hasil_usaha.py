from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskukOpendataKeragaanKoperasiBerdasarkanSisaHasilUsahaModel(BaseModel, db.Model):
    """Model for the diskuk_opendata_keragaan_koperasi_berdasarkan_sisa_hasil_usaha table"""
    __tablename__ = 'opendata_keragaan_koperasi_berdasarkan_sisa_hasil_usaha'
    __table_args__ = {'schema': 'diskuk'}

    # start model
    provinsi = db.Column(db.String(255))
    kabupaten_atau_kota = db.Column(db.String(255))
    kode_kabupaten_atau_kota = db.Column(db.Integer)
    sisa_hasil_usaha = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
