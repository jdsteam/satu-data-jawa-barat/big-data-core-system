from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class JqrOpendataLaporanAduanKategoriModel(BaseModel, db.Model):
    """Model for the jqr_opendata_laporan_aduan_kategori table"""
    __tablename__ = 'opendata_laporan_aduan_kategori'
    __table_args__ = {'schema': 'jqr'}

    # start model
    kategori = db.Column(db.String(255))
    channel = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
