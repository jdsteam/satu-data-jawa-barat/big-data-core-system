from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BapendaLaporanpajakPerlokasiModel(BaseModel, db.Model):
    """Model for the bapenda_laporanpajak_perlokasi table"""
    __tablename__ = 'laporanpajak_perlokasi'
    __table_args__ = {'schema': 'bapenda'}

    # start model
    idlokasi = db.Column(db.Integer)
    tahun = db.Column(db.Integer)
    idbulan = db.Column(db.Integer)
    jenis_kendaraan_bermotor = db.Column(db.String(255))
    type = db.Column(db.String(255))
    pkb_kbm = db.Column(db.Integer)
    pkb_jumlah = db.Column(db.Integer)
    id = db.Column(db.Integer, primary_key=True)
