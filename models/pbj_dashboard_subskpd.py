from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardSubskpdModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_subskpd table"""
    __tablename__ = 'dashboard_subskpd'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    kodesubskpd = db.Column(db.String(255))
    namasubskpd = db.Column(db.String(255))
    idskpd = db.Column(db.Integer)
