from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisparbudOpendataUsahaJasaKonsultanDanInformasiPariwisataModel(BaseModel, db.Model):
    """Model for the disparbud_opendata_usaha_jasa_konsultan_dan_informasi_pariwisata table"""
    __tablename__ = 'opendata_usaha_jasa_konsultan_dan_informasi_pariwisata'
    __table_args__ = {'schema': 'disparbud'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    jasa = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
