from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjFactApbdModel(BaseModel, db.Model):
    """Model for the pbj_fact_apbd table"""
    __tablename__ = 'fact_apbd'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idbl = db.Column(db.Integer)
    tahun = db.Column(db.Integer)
    idurusan = db.Column(db.Integer)
    idskpd = db.Column(db.Integer)
    idskpdsub = db.Column(db.Integer)
    idprogram = db.Column(db.Integer)
    idgiat = db.Column(db.Integer)
    pagu = db.Column(db.Float)
    id = db.Column(db.Integer, primary_key=True)
