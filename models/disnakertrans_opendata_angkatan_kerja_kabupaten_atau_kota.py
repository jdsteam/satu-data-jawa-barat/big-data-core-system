from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisnakertransOpendataAngkatanKerjaKabupatenAtauKotaModel(BaseModel, db.Model):
    """Model for the disnakertrans_opendata_angkatan_kerja_kabupaten_atau_kota table"""
    __tablename__ = 'opendata_angkatan_kerja_kabupaten_atau_kota'
    __table_args__ = {'schema': 'disnakertrans'}

    # start model
    provinsi = db.Column(db.String(255))
    kabupaten_atau_kota = db.Column(db.String(255))
    kode_kabupaten_atau_kota = db.Column(db.Integer)
    angkatan_kerja = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
