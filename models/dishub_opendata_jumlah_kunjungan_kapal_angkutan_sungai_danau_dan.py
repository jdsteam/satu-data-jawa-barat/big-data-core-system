from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DishubOpendataJumlahKunjunganKapalAngkutanSungaiDanauDanModel(BaseModel, db.Model):
    """Model for the dishub_opendata_jumlah_kunjungan_kapal_angkutan_sungai_danau_dan table"""
    __tablename__ = 'opendata_jumlah_kunjungan_kapal_angkutan_sungai_danau_dan'
    __table_args__ = {'schema': 'dishub'}

    # start model
    sub_unit_pelayanan_llasdp = db.Column(db.String(255))
    kunjungan_kapal = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
