from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class JshOpendataAduanMasukSemuaPlatformMedsosModel(BaseModel, db.Model):
    """Model for the jsh_opendata_aduan_masuk_semua_platform_medsos table"""
    __tablename__ = 'opendata_aduan_masuk_semua_platform_medsos'
    __table_args__ = {'schema': 'jsh'}

    # start model
    channel = db.Column(db.String(255))
    kategori_aduan = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
