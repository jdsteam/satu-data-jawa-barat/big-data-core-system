from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpsPodes201832KabupatenModel(BaseModel, db.Model):
    """Model for the bps_podes2018_32_kabupaten table"""
    __tablename__ = 'podes2018_32_kabupaten'
    __table_args__ = {'schema': 'bps'}

    # start model
    R101 = db.Column(db.String(255))
    R101N = db.Column(db.String(255))
    R102 = db.Column(db.String(255))
    R102N = db.Column(db.String(255))
    R303 = db.Column(db.String(255))
    R401AK2 = db.Column(db.String(255))
    R401BK2 = db.Column(db.String(255))
    R501AK2 = db.Column(db.String(255))
    R501AK3 = db.Column(db.String(255))
    R501BK2 = db.Column(db.String(255))
    R501BK3 = db.Column(db.String(255))
    R601A = db.Column(db.String(255))
    R601B = db.Column(db.String(255))
    R701AK2 = db.Column(db.String(255))
    R701AK3 = db.Column(db.String(255))
    R701BK2 = db.Column(db.String(255))
    R701BK3 = db.Column(db.String(255))
    R701CK2 = db.Column(db.String(255))
    R701CK3 = db.Column(db.String(255))
    R701DK2 = db.Column(db.String(255))
    R701DK3 = db.Column(db.String(255))
    R801A = db.Column(db.String(255))
    R801B = db.Column(db.String(255))
    R901AK2 = db.Column(db.String(255))
    R901AK3 = db.Column(db.String(255))
    R901AK4 = db.Column(db.String(255))
    R901AK5 = db.Column(db.String(255))
    R901BK2 = db.Column(db.String(255))
    R901BK3 = db.Column(db.String(255))
    R901BK4 = db.Column(db.String(255))
    R901BK5 = db.Column(db.String(255))
    R901CK2 = db.Column(db.String(255))
    R901CK3 = db.Column(db.String(255))
    R901CK4 = db.Column(db.String(255))
    R901CK5 = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
