from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaModel(BaseModel, db.Model):
    """Model for the dkpp_jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_ table"""
    __tablename__ = 'jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_'
    __table_args__ = {'schema': 'dkpp'}

    # start model
    provinsi = db.Column(db.String(20))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(50))
    hewan_ternak = db.Column(db.String(30))
    jenis_kelamin = db.Column(db.String(20))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(10))
    tahun = db.Column(db.String(30))
    id = db.Column(db.Integer, primary_key=True)
