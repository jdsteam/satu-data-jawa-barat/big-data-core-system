from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimGiatModel(BaseModel, db.Model):
    """Model for the pbj_dim_giat table"""
    __tablename__ = 'dim_giat'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idgiat = db.Column(db.Integer)
    tahun = db.Column(db.Integer)
    iddaerah = db.Column(db.Integer)
    namagiat = db.Column(db.String(255))
    isgiatumum = db.Column(db.Float)
    idjenisgiat = db.Column(db.Float)
    islocked = db.Column(db.Integer)
    id = db.Column(db.Integer, primary_key=True)
