from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DlhOpendataJumlahSampahMasukKePengelolaanKomposSarimuktiModel(BaseModel, db.Model):
    """Model for the dlh_opendata_jumlah_sampah_masuk_ke_pengelolaan_kompos_sarimukti table"""
    __tablename__ = 'opendata_jumlah_sampah_masuk_ke_pengelolaan_kompos_sarimukti'
    __table_args__ = {'schema': 'dlh'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    kota_kabupaten = db.Column(db.String(255))
    bulan = db.Column(db.String(255))
    jumlah = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
