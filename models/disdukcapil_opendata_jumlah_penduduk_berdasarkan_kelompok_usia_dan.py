from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisdukcapilOpendataJumlahPendudukBerdasarkanKelompokUsiaDanModel(BaseModel, db.Model):
    """Model for the disdukcapil_opendata_jumlah_penduduk_berdasarkan_kelompok_usia_dan table"""
    __tablename__ = 'opendata_jumlah_penduduk_berdasarkan_kelompok_usia_dan'
    __table_args__ = {'schema': 'disdukcapil'}

    # start model
    kode_kota_kabupaten = db.Column(db.Integer)
    kota_kabupaten = db.Column(db.String(255))
    jenis_kelamin = db.Column(db.String(255))
    kelompok_usia = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
