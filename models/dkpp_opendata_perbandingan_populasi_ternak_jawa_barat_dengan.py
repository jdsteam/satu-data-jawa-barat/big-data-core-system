from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DkppOpendataPerbandinganPopulasiTernakJawaBaratDenganModel(BaseModel, db.Model):
    """Model for the dkpp_opendata_perbandingan_populasi_ternak_jawa_barat_dengan table"""
    __tablename__ = 'opendata_perbandingan_populasi_ternak_jawa_barat_dengan'
    __table_args__ = {'schema': 'dkpp'}

    # start model
    jenis_ternak = db.Column(db.String(255))
    wilayah = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
