from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpkadOpendataJumlahAnggaranBelanjaLangsungJawaBaratModel(BaseModel, db.Model):
    """Model for the bpkad_opendata_jumlah_anggaran_belanja_langsung_jawa_barat table"""
    __tablename__ = 'opendata_jumlah_anggaran_belanja_langsung_jawa_barat'
    __table_args__ = {'schema': 'bpkad'}

    # start model
    anggaran_belanja = db.Column(db.String(255))
    jenis_anggaran = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
