from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjFactSipkdModel(BaseModel, db.Model):
    """Model for the pbj_fact_sipkd table"""
    __tablename__ = 'fact_sipkd'
    __table_args__ = {'schema': 'pbj'}

    # start model
    tahun = db.Column(db.Integer)
    idskpdsub = db.Column(db.Integer)
    idskpd = db.Column(db.Integer)
    idbl = db.Column(db.Integer)
    jenis = db.Column(db.String(255))
    bulan1 = db.Column(db.Float)
    bulan2 = db.Column(db.Float)
    bulan3 = db.Column(db.Float)
    bulan4 = db.Column(db.Float)
    bulan5 = db.Column(db.Float)
    bulan6 = db.Column(db.Float)
    bulan7 = db.Column(db.Float)
    bulan8 = db.Column(db.Float)
    bulan9 = db.Column(db.Float)
    bulan10 = db.Column(db.Float)
    bulan11 = db.Column(db.Float)
    bulan12 = db.Column(db.Float)
    createdat = db.Column(db.DateTime(True))
    updatedat = db.Column(db.DateTime(True))
    id = db.Column(db.Integer, primary_key=True)
