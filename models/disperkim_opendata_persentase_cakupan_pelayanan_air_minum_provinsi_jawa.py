from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisperkimOpendataPersentaseCakupanPelayananAirMinumProvinsiJawaModel(BaseModel, db.Model):
    """Model for the disperkim_opendata_persentase_cakupan_pelayanan_air_minum_provinsi_jawa table"""
    __tablename__ = 'opendata_persentase_cakupan_pelayanan_air_minum_provinsi_jawa'
    __table_args__ = {'schema': 'disperkim'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    cakupan = db.Column(db.String(255))
    presentase = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
