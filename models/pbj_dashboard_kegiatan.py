from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardKegiatanModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_kegiatan table"""
    __tablename__ = 'dashboard_kegiatan'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idbl = db.Column(db.Integer, primary_key=True)
    idkegiatan = db.Column(db.Integer)
    tahun = db.Column(db.Integer)
    kodekegiatan = db.Column(db.String(30))
    kodekegiatanpaket = db.Column(db.String(255))
    namakegiatan = db.Column(db.String(255))
    idprogram = db.Column(db.Integer)
    pagu = db.Column(db.Float)
