from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimUrusanModel(BaseModel, db.Model):
    """Model for the pbj_dim_urusan table"""
    __tablename__ = 'dim_urusan'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idurusan = db.Column(db.Integer)
    tahun = db.Column(db.Integer)
    iddaerah = db.Column(db.Integer)
    idurusaninduk = db.Column(db.Integer)
    idfungsi = db.Column(db.Integer)
    kodeurusan = db.Column(db.String(255))
    namaurusan = db.Column(db.String(255))
    islocked = db.Column(db.Integer)
    nourusan = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
