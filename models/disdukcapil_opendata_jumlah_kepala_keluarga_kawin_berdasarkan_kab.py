from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisdukcapilOpendataJumlahKepalaKeluargaKawinBerdasarkanKabModel(BaseModel, db.Model):
    """Model for the disdukcapil_opendata_jumlah_kepala_keluarga_kawin_berdasarkan_kab table"""
    __tablename__ = 'opendata_jumlah_kepala_keluarga_kawin_berdasarkan_kab'
    __table_args__ = {'schema': 'disdukcapil'}

    # start model
    kode_kota_kabupaten = db.Column(db.Integer)
    kota_kabupaten = db.Column(db.String(255))
    jenis_kelamin = db.Column(db.String(255))
    jumlah_kepala_keluarga = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
