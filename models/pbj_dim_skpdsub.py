from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimSkpdsubModel(BaseModel, db.Model):
    """Model for the pbj_dim_skpdsub table"""
    __tablename__ = 'dim_skpdsub'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idskpdsub = db.Column(db.Integer)
    tahun = db.Column(db.Integer)
    iddaerah = db.Column(db.Integer)
    idskpd = db.Column(db.Integer)
    kodeunitsub = db.Column(db.String(255))
    kodeskpdsub = db.Column(db.String(255))
    namaskpdsub = db.Column(db.String(255))
    noskpdsub = db.Column(db.String(255))
    isppkd = db.Column(db.Integer)
    isskpd = db.Column(db.Integer)
    isinput = db.Column(db.Integer)
    komisi = db.Column(db.String(255))
    namakepala = db.Column(db.String(255))
    nipkepala = db.Column(db.String(255))
    pangkatkepala = db.Column(db.String(255))
    namabendahara = db.Column(db.String(255))
    nipbendahara = db.Column(db.String(255))
    nodpa = db.Column(db.String(255))
    namaplh = db.Column(db.String(255))
    nipplh = db.Column(db.String(255))
    pangkatplh = db.Column(db.String(255))
    namaplt = db.Column(db.String(255))
    nipplt = db.Column(db.String(255))
    pangkatplt = db.Column(db.String(255))
    updateduser = db.Column(db.Float)
    updatedat = db.Column(db.DateTime(True))
    updatedip = db.Column(db.String(255))
    subopdsipkdteks = db.Column(db.String(255))
    islocked = db.Column(db.Integer)
    id = db.Column(db.Integer, primary_key=True)
