from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimProgramModel(BaseModel, db.Model):
    """Model for the pbj_dim_program table"""
    __tablename__ = 'dim_program'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idprogram = db.Column(db.Integer)
    tahun = db.Column(db.Integer)
    iddaerah = db.Column(db.Integer)
    idurusan = db.Column(db.Integer)
    kodeprogram = db.Column(db.String(255))
    namaprogram = db.Column(db.String(255))
    noprogram = db.Column(db.String(255))
    islocked = db.Column(db.Integer)
    id = db.Column(db.Integer, primary_key=True)
