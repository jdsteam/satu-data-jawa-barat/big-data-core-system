from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDwhDimPaketModel(BaseModel, db.Model):
    """Model for the pbj_dwh_dim_paket table"""
    __tablename__ = 'dwh_dim_paket'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idpaket = db.Column(db.Float)
    namapaket = db.Column(db.String(255))
    jumlahpagu = db.Column(db.Float)
    id = db.Column(db.Integer, primary_key=True)
