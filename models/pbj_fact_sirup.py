from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjFactSirupModel(BaseModel, db.Model):
    """Model for the pbj_fact_sirup table"""
    __tablename__ = 'fact_sirup'
    __table_args__ = {'schema': 'pbj'}

    # start model
    awalpekerjaan = db.Column(db.String(255))
    akhirpekerjaan = db.Column(db.String(255))
    awalpengadaan = db.Column(db.String(255))
    akhirpengadaan = db.Column(db.String(255))
    idsatker = db.Column(db.Integer)
    idswakelola = db.Column(db.Float)
    idrup = db.Column(db.Integer)
    idkldi = db.Column(db.String(255))
    kodesatker = db.Column(db.String(255))
    kodeprogram = db.Column(db.String(255))
    kodegiat = db.Column(db.String(255))
    mak = db.Column(db.String(255))
    lokasi = db.Column(db.String(255))
    jenispengadaan = db.Column(db.String(255))
    metodepemilihan = db.Column(db.String(255))
    namapaket = db.Column(db.String(255))
    pagu_rup = db.Column(db.Integer)
    penyedia_didalam_swakelola = db.Column(db.String(255))
    pradipa = db.Column(db.String(255))
    spesifikasi = db.Column(db.String(255))
    status_aktif = db.Column(db.String(255))
    status_umumkan = db.Column(db.String(255))
    sumberdana = db.Column(db.String(255))
    tkdn = db.Column(db.String(255))
    volume = db.Column(db.String(255))
    label = db.Column(db.String(255))
    tipeswakelola = db.Column(db.Float)
    id = db.Column(db.Integer, primary_key=True)
