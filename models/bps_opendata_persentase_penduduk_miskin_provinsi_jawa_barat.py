from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpsOpendataPersentasePendudukMiskinProvinsiJawaBaratModel(BaseModel, db.Model):
    """Model for the bps_opendata_persentase_penduduk_miskin_provinsi_jawa_barat table"""
    __tablename__ = 'opendata_persentase_penduduk_miskin_provinsi_jawa_barat'
    __table_args__ = {'schema': 'bps'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    penduduk_miskin = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
