from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BapendaBulanModel(BaseModel, db.Model):
    """Model for the bapenda_bulan table"""
    __tablename__ = 'bulan'
    __table_args__ = {'schema': 'bapenda'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    nama = db.Column(db.String(255))
