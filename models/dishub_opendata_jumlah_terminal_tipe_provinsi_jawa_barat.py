from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DishubOpendataJumlahTerminalTipeProvinsiJawaBaratModel(BaseModel, db.Model):
    """Model for the dishub_opendata_jumlah_terminal_tipe_provinsi_jawa_barat table"""
    __tablename__ = 'opendata_jumlah_terminal_tipe_provinsi_jawa_barat'
    __table_args__ = {'schema': 'dishub'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    tipe_terminal = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
