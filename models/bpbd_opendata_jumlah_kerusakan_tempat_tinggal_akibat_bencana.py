from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpbdOpendataJumlahKerusakanTempatTinggalAkibatBencanaModel(BaseModel, db.Model):
    """Model for the bpbd_opendata_jumlah_kerusakan_tempat_tinggal_akibat_bencana table"""
    __tablename__ = 'opendata_jumlah_kerusakan_tempat_tinggal_akibat_bencana'
    __table_args__ = {'schema': 'bpbd'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    kota_kabupaten = db.Column(db.String(255))
    kondisi = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
