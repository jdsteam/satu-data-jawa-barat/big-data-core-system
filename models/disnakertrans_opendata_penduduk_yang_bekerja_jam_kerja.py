from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisnakertransOpendataPendudukYangBekerjaJamKerjaModel(BaseModel, db.Model):
    """Model for the disnakertrans_opendata_penduduk_yang_bekerja_jam_kerja table"""
    __tablename__ = 'opendata_penduduk_yang_bekerja_jam_kerja'
    __table_args__ = {'schema': 'disnakertrans'}

    # start model
    jam_kerja = db.Column(db.String(255))
    penduduk_yang_bekerja = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
