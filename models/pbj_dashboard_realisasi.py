from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardRealisasiModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_realisasi table"""
    __tablename__ = 'dashboard_realisasi'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idbl = db.Column(db.Integer, primary_key=True)
    tahun = db.Column(db.Integer)
    jenis = db.Column(db.String(255))
    bulan1 = db.Column(db.Float)
    bulan2 = db.Column(db.Float)
    bulan3 = db.Column(db.Float)
    bulan4 = db.Column(db.Float)
    bulan5 = db.Column(db.Float)
    bulan6 = db.Column(db.Float)
    bulan7 = db.Column(db.Float)
    bulan8 = db.Column(db.Float)
    bulan9 = db.Column(db.Float)
    bulan10 = db.Column(db.Float)
    bulan11 = db.Column(db.Float)
    bulan12 = db.Column(db.Float)
