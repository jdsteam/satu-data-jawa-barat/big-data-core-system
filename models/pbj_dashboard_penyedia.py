from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardPenyediaModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_penyedia table"""
    __tablename__ = 'dashboard_penyedia'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    namapenyedia = db.Column(db.String(255))
    tipepenyedia = db.Column(db.String(255))
    description = db.Column(db.String(255))
    join_date = db.Column(db.DateTime(True))
    expiry_date = db.Column(db.DateTime(True))
