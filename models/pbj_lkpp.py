from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjLkppModel(BaseModel, db.Model):
    """Model for the pbj_lkpp table"""
    __tablename__ = 'lkpp'
    __table_args__ = {'schema': 'pbj'}

    # start model
    kodetender = db.Column(db.Float)
    namatender = db.Column(db.String(255))
    koderup = db.Column(db.Float)
    tanggalpembuatan = db.Column(db.String(255))
    keterangan = db.Column(db.String(255))
    tahaptendersaatini = db.Column(db.String(255))
    instansi = db.Column(db.String(255))
    satuankerja = db.Column(db.String(255))
    kategori = db.Column(db.String(255))
    sistempengadaan = db.Column(db.String(255))
    tahunanggaran = db.Column(db.Integer)
    nilaipagupaket = db.Column(db.Float)
    jeniskontrak = db.Column(db.String(255))
    lokasipekerjaan = db.Column(db.String(255))
    kualifikasiusaha = db.Column(db.String(255))
    nama = db.Column(db.String(255))
    pesertatender = db.Column(db.Integer)
    id = db.Column(db.Integer, primary_key=True)
