from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class MetadataOperationalIngestionInfoModel(BaseModel, db.Model):
    """Model for the metadata_operational_ingestion_info table"""
    __tablename__ = 'operational_ingestion_info'
    __table_args__ = {'schema': 'metadata'}

    # start model
    object_type = db.Column(db.String(255))
    url = db.Column(db.String(255))
    object_source = db.Column(db.String(255))
    object_target = db.Column(db.String(255))
    start_date = db.Column(db.DateTime(True))
    end_date = db.Column(db.DateTime(True))
    duration = db.Column(db.Float)
    status = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
