from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class Ban_PtAkreditasiInstitusiModel(BaseModel, db.Model):
    """Model for the ban_pt_akreditasi_institusi table"""
    __tablename__ = 'akreditasi_institusi'
    __table_args__ = {'schema': 'ban_pt'}

    # start model
    perguruan_tinggi = db.Column(db.String(100))
    peringkat = db.Column(db.String(50))
    sk = db.Column(db.String(100))
    tahun_sk = db.Column(db.Integer)
    wilayah = db.Column(db.Integer)
    tanggal_kadaluarsa = db.Column(db.String(50))
    status_kadaluarsa = db.Column(db.String(100))
    id = db.Column(db.Integer, primary_key=True)
