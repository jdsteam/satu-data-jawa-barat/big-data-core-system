from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisparbudOpendataMasyarakatPariwisataIndonesiaAnggotaProvinsiModel(BaseModel, db.Model):
    """Model for the disparbud_opendata_masyarakat_pariwisata_indonesia_anggota_provinsi table"""
    __tablename__ = 'opendata_masyarakat_pariwisata_indonesia_anggota_provinsi'
    __table_args__ = {'schema': 'disparbud'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    anggota = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
