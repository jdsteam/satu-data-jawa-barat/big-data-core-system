from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskukOpendataKoperasiAktifBerdasarkanKabupatenAtauKotaModel(BaseModel, db.Model):
    """Model for the diskuk_opendata_koperasi_aktif_berdasarkan_kabupaten_atau_kota table"""
    __tablename__ = 'opendata_koperasi_aktif_berdasarkan_kabupaten_atau_kota'
    __table_args__ = {'schema': 'diskuk'}

    # start model
    provinsi = db.Column(db.String(255))
    kabupaten_atau_kota = db.Column(db.String(255))
    kode_kabupaten_atau_kota = db.Column(db.Integer)
    keaktifan_koperasi = db.Column(db.String(255))
    koperasi = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
