from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class Bp2DOpendataPenelitianDanKajianBp2DJawaBaratModel(BaseModel, db.Model):
    """Model for the bp2d_opendata_penelitian_dan_kajian_bp2d_jawa_barat table"""
    __tablename__ = 'opendata_penelitian_dan_kajian_bp2d_jawa_barat'
    __table_args__ = {'schema': 'bp2d'}

    # start model
    judul_penelitian = db.Column(db.String(255))
    narasumber = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
