from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpsOpendataIndeksKesehatanBerdasarkanProvinsiModel(BaseModel, db.Model):
    """Model for the bps_opendata_indeks_kesehatan_berdasarkan_provinsi table"""
    __tablename__ = 'opendata_indeks_kesehatan_berdasarkan_provinsi'
    __table_args__ = {'schema': 'bps'}

    # start model
    kode_provinsi = db.Column(db.Integer)
    provinsi = db.Column(db.String(255))
    indeks_kesehatan = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
