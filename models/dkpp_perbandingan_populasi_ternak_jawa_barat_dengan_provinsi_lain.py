from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DkppPerbandinganPopulasiTernakJawaBaratDenganProvinsiLainModel(BaseModel, db.Model):
    """Model for the dkpp_perbandingan_populasi_ternak_jawa_barat_dengan_provinsi_lain table"""
    __tablename__ = 'perbandingan_populasi_ternak_jawa_barat_dengan_provinsi_lain'
    __table_args__ = {'schema': 'dkpp'}

    # start model
    jenis_ternak = db.Column(db.String(20))
    wilayah = db.Column(db.String(20))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(20))
    tahun = db.Column(db.String(30))
    id = db.Column(db.Integer, primary_key=True)
