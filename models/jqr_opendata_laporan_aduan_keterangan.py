from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class JqrOpendataLaporanAduanKeteranganModel(BaseModel, db.Model):
    """Model for the jqr_opendata_laporan_aduan_keterangan table"""
    __tablename__ = 'opendata_laporan_aduan_keterangan'
    __table_args__ = {'schema': 'jqr'}

    # start model
    keterangan = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
