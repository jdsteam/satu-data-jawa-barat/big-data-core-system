from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BapendaLaporanpajakModel(BaseModel, db.Model):
    """Model for the bapenda_laporanpajak table"""
    __tablename__ = 'laporanpajak'
    __table_args__ = {'schema': 'bapenda'}

    # start model
    idlokasi = db.Column(db.Integer)
    tanggal = db.Column(db.DateTime(True))
    nama_wilayah_proses = db.Column(db.String(255))
    kbm = db.Column(db.Integer)
    bbnkb_1_pok = db.Column(db.Integer)
    bbnkb_1_den = db.Column(db.Integer)
    bbnkb_2_pok = db.Column(db.Integer)
    bbnkb_2_den = db.Column(db.Integer)
    pkb_pok = db.Column(db.Integer)
    pkb_den = db.Column(db.Integer)
    jumlah = db.Column(db.Integer)
    id = db.Column(db.Integer, primary_key=True)
