from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DistanhorOpendataTanamanBuahModel(BaseModel, db.Model):
    """Model for the distanhor_opendata_tanaman_buah table"""
    __tablename__ = 'opendata_tanaman_buah'
    __table_args__ = {'schema': 'distanhor'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    kota_kabupaten = db.Column(db.String(255))
    jenis_komoditi = db.Column(db.String(255))
    jumlah_tanaman = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
