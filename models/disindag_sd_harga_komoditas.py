from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisindagSdHargaKomoditasModel(BaseModel, db.Model):
    """Model for the disindag_sd_harga_komoditas table"""
    __tablename__ = 'sd_harga_komoditas'
    __table_args__ = {'schema': 'disindag'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    tanggal = db.Column(db.String(40))
    nama_komoditas = db.Column(db.String(60))
    unit = db.Column(db.String(30))
    harga = db.Column(db.Integer)
    kategori_nama = db.Column(db.String(50))
    persentasi_kenaikan = db.Column(db.Float)
    tanggal_pembanding = db.Column(db.String(40))
