from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BapendaPotensipajakUmumModel(BaseModel, db.Model):
    """Model for the bapenda_potensipajak_umum table"""
    __tablename__ = 'potensipajak_umum'
    __table_args__ = {'schema': 'bapenda'}

    # start model
    idbulan = db.Column(db.Integer)
    idlokasi = db.Column(db.Integer)
    idpenerimaan = db.Column(db.Integer)
    jenispenerimaan = db.Column(db.String(255))
    koderekening = db.Column(db.String(255))
    persentase = db.Column(db.String(255))
    realisasi = db.Column(db.Integer)
    target = db.Column(db.Integer)
    id = db.Column(db.Integer, primary_key=True)
    tahun = db.Column(db.Integer)
