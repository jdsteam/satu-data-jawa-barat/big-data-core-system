from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardSkpdModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_skpd table"""
    __tablename__ = 'dashboard_skpd'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    kodeskpd = db.Column(db.String(255))
    namaskpd = db.Column(db.String(255))
    idbidang = db.Column(db.Integer)
