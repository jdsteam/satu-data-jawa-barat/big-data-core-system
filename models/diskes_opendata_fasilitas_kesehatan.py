from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskesOpendataFasilitasKesehatanModel(BaseModel, db.Model):
    """Model for the diskes_opendata_fasilitas_kesehatan table"""
    __tablename__ = 'opendata_fasilitas_kesehatan'
    __table_args__ = {'schema': 'diskes'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    id_tipe_faskes = db.Column(db.Integer)
    kode_unit_dinkes = db.Column(db.String(50))
    nama_faskes = db.Column(db.String(400))
    kode_kab_bps = db.Column(db.String(100))
    nama_kab = db.Column(db.String(200))
    nama_kec = db.Column(db.String(200))
    nama_kel = db.Column(db.String(200))
    alamat = db.Column(db.String(600))
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    nomor_rujukan_covid19 = db.Column(db.Integer)
