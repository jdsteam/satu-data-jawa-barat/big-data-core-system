from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskesOpendataOdpPdpPositifCovid19JawaBaratModel(BaseModel, db.Model):
    """Model for the diskes_opendata_odp_pdp_positif_covid19_jawa_barat table"""
    __tablename__ = 'opendata_odp_pdp_positif_covid19_jawa_barat'
    __table_args__ = {'schema': 'diskes'}

    # start model
    id = db.Column(db.String(50), primary_key=True)
    kode_kab = db.Column(db.String(50))
    nama_kab = db.Column(db.String(100))
    kode_kec = db.Column(db.String(50))
    nama_kec = db.Column(db.String(100))
    kode_kel = db.Column(db.String(50))
    nama_kel = db.Column(db.String(100))
    status = db.Column(db.String(100))
    stage = db.Column(db.String(100))
    umur = db.Column(db.Integer)
    gender = db.Column(db.String(50))
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    tanggal_konfirmasi = db.Column(db.DateTime(True))
    tanggal_update = db.Column(db.DateTime(True))
