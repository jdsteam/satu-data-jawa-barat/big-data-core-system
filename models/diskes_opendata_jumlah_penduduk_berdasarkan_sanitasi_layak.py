from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskesOpendataJumlahPendudukBerdasarkanSanitasiLayakModel(BaseModel, db.Model):
    """Model for the diskes_opendata_jumlah_penduduk_berdasarkan_sanitasi_layak table"""
    __tablename__ = 'opendata_jumlah_penduduk_berdasarkan_sanitasi_layak'
    __table_args__ = {'schema': 'diskes'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    jenis_sarana = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
