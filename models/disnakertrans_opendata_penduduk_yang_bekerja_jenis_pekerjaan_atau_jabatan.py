from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisnakertransOpendataPendudukYangBekerjaJenisPekerjaanAtauJabatanModel(BaseModel, db.Model):
    """Model for the disnakertrans_opendata_penduduk_yang_bekerja_jenis_pekerjaan_atau_jabatan table"""
    __tablename__ = 'opendata_penduduk_yang_bekerja_jenis_pekerjaan_atau_jabatan'
    __table_args__ = {'schema': 'disnakertrans'}

    # start model
    jenis_pekerjaan_atau_jabatan = db.Column(db.String(255))
    penduduk_yang_bekerja = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
