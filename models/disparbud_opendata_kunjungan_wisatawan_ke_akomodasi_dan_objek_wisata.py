from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisparbudOpendataKunjunganWisatawanKeAkomodasiDanObjekWisataModel(BaseModel, db.Model):
    """Model for the disparbud_opendata_kunjungan_wisatawan_ke_akomodasi_dan_objek_wisata table"""
    __tablename__ = 'opendata_kunjungan_wisatawan_ke_akomodasi_dan_objek_wisata'
    __table_args__ = {'schema': 'disparbud'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    wisatawan = db.Column(db.String(255))
    wisata = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
