from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DkppOpendataJumlahPemasukanDombaKeProvinsiJawaBaratModel(BaseModel, db.Model):
    """Model for the dkpp_opendata_jumlah_pemasukan_domba_ke_provinsi_jawa_barat table"""
    __tablename__ = 'opendata_jumlah_pemasukan_domba_ke_provinsi_jawa_barat'
    __table_args__ = {'schema': 'dkpp'}

    # start model
    provinsi = db.Column(db.String(20))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(50))
    jenis = db.Column(db.String(20))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(10))
    tahun = db.Column(db.Integer)
    id = db.Column(db.Integer, primary_key=True)
