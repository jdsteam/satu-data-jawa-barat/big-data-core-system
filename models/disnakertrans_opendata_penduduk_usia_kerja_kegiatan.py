from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisnakertransOpendataPendudukUsiaKerjaKegiatanModel(BaseModel, db.Model):
    """Model for the disnakertrans_opendata_penduduk_usia_kerja_kegiatan table"""
    __tablename__ = 'opendata_penduduk_usia_kerja_kegiatan'
    __table_args__ = {'schema': 'disnakertrans'}

    # start model
    kegiatan = db.Column(db.String(255))
    penduduk_usia_kerja = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
