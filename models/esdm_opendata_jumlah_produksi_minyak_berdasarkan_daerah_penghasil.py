from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class EsdmOpendataJumlahProduksiMinyakBerdasarkanDaerahPenghasilModel(BaseModel, db.Model):
    """Model for the esdm_opendata_jumlah_produksi_minyak_berdasarkan_daerah_penghasil table"""
    __tablename__ = 'opendata_jumlah_produksi_minyak_berdasarkan_daerah_penghasil'
    __table_args__ = {'schema': 'esdm'}

    # start model
    daerah_penghasil = db.Column(db.String(255))
    jumlah_produksi = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
