from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BkdOpendataJumlahPnsDaerahOtonomBerdasarkanJenisKelaminModel(BaseModel, db.Model):
    """Model for the bkd_opendata_jumlah_pns_daerah_otonom_berdasarkan_jenis_kelamin table"""
    __tablename__ = 'opendata_jumlah_pns_daerah_otonom_berdasarkan_jenis_kelamin'
    __table_args__ = {'schema': 'bkd'}

    # start model
    unit_kerja = db.Column(db.String(255))
    jenis_kelamin = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
