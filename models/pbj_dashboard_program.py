from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardProgramModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_program table"""
    __tablename__ = 'dashboard_program'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    tahun = db.Column(db.Integer)
    kodeprogram = db.Column(db.String(255))
    namaprogram = db.Column(db.String(255))
    idskpd = db.Column(db.Integer)
