from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskesOdpPdpPositifCovid19DetailJawaBaratModel(BaseModel, db.Model):
    """Model for the diskes_odp_pdp_positif_covid19_detail_jawa_barat table"""
    __tablename__ = 'odp_pdp_positif_covid19_detail_jawa_barat'
    __table_args__ = {'schema': 'diskes'}

    # start model
    id = db.Column(db.String(50), primary_key=True)
    kode_kab = db.Column(db.String(50))
    nama_kab = db.Column(db.String(100))
    kode_kec = db.Column(db.String(50))
    nama_kec = db.Column(db.String(100))
    kode_kel = db.Column(db.String(50))
    nama_kel = db.Column(db.String(100))
    status = db.Column(db.String(100))
    stage = db.Column(db.String(100))
    umur = db.Column(db.Integer)
    gender = db.Column(db.String(50))
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    tanggal_konfirmasi = db.Column(db.DateTime(True))
    tanggal_update = db.Column(db.DateTime(True))
    current_location_type = db.Column(db.String(100))
    current_location_district_code = db.Column(db.String(50))
    current_location_subdistrict_code = db.Column(db.String(50))
    current_location_village_code = db.Column(db.String(50))
    current_location_address = db.Column(db.String(500))
    report_source = db.Column(db.String(100))
