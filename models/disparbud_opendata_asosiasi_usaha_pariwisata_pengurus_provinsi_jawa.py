from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisparbudOpendataAsosiasiUsahaPariwisataPengurusProvinsiJawaModel(BaseModel, db.Model):
    """Model for the disparbud_opendata_asosiasi_usaha_pariwisata_pengurus_provinsi_jawa table"""
    __tablename__ = 'opendata_asosiasi_usaha_pariwisata_pengurus_provinsi_jawa'
    __table_args__ = {'schema': 'disparbud'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    asosiasi = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
