from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardBidangModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_bidang table"""
    __tablename__ = 'dashboard_bidang'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    kodebidang = db.Column(db.String)
    namabidang = db.Column(db.String(255))
    idurusan = db.Column(db.Integer)
