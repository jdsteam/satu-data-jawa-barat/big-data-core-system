from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class TemplateModel(BaseModel, db.Model):
    """Model for the template table"""
    __tablename__ = 'template'
    __table_args__ = {'schema': 'public'}

    # start model
    id = db.Column(db.Integer, primary_key=True) 