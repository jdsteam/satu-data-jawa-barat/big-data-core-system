from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DistanhorOpendataProduksiBuahMenurutKotaKabJawaBaratModel(BaseModel, db.Model):
    """Model for the distanhor_opendata_produksi_buah_menurut_kota_kab_jawa_barat table"""
    __tablename__ = 'opendata_produksi_buah_menurut_kota_kab_jawa_barat'
    __table_args__ = {'schema': 'distanhor'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    jenis_komoditi = db.Column(db.String(255))
    jumlah_produksi = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
