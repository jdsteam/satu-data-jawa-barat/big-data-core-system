from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimKategoriModel(BaseModel, db.Model):
    """Model for the pbj_dim_kategori table"""
    __tablename__ = 'dim_kategori'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idkategori = db.Column(db.Integer)
    kategori = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
