from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisdukcapilOpendataJumlahPendudukBerdasarkanGolonganDarahDanJenisModel(BaseModel, db.Model):
    """Model for the disdukcapil_opendata_jumlah_penduduk_berdasarkan_golongan_darah_dan_jenis table"""
    __tablename__ = 'opendata_jumlah_penduduk_berdasarkan_golongan_darah_dan_jenis'
    __table_args__ = {'schema': 'disdukcapil'}

    # start model
    golongan_darah = db.Column(db.String(255))
    laki_laki = db.Column(db.Integer)
    perempuan = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
