from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DkppOpendataJumlahPemotonganHewanTernakRphPemerintahModel(BaseModel, db.Model):
    """Model for the dkpp_opendata_jumlah_pemotongan_hewan_ternak_rph_pemerintah table"""
    __tablename__ = 'opendata_jumlah_pemotongan_hewan_ternak_rph_pemerintah'
    __table_args__ = {'schema': 'dkpp'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    hewan_ternak = db.Column(db.String(255))
    jenis_kelamin = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
