from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimDateModel(BaseModel, db.Model):
    """Model for the pbj_dim_date table"""
    __tablename__ = 'dim_date'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime(True))
    day = db.Column(db.Integer)
    month = db.Column(db.Integer)
    year = db.Column(db.Integer)
    quarter = db.Column(db.Integer)
    week = db.Column(db.Integer)
    weekday_en = db.Column(db.String(255))
    month_en = db.Column(db.String(255))
    weekday_id = db.Column(db.String(255))
