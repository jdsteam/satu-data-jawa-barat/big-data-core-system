from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisnakertransOpendataTingkatPartisipasiAngkatanKerjaGolonganUmurJawaModel(BaseModel, db.Model):
    """Model for the disnakertrans_opendata_tingkat_partisipasi_angkatan_kerja_golongan_umur_jawa table"""
    __tablename__ = 'opendata_tingkat_partisipasi_angkatan_kerja_golongan_umur_jawa'
    __table_args__ = {'schema': 'disnakertrans'}

    # start model
    golongan_umur = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
