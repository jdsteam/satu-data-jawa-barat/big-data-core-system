from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class AliasAliasModel(BaseModel, db.Model):
    """Model for the alias_alias table"""
    __tablename__ = 'alias'
    __table_args__ = {'schema': 'alias'}

    # start model
    idskpd = db.Column(db.Integer)
    alias = db.Column(db.String)
    namaskpd = db.Column(db.String)
    id = db.Column(db.Integer, primary_key=True)
