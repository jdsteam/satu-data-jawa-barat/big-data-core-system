from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardSatuankerjaModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_satuankerja table"""
    __tablename__ = 'dashboard_satuankerja'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    namasatker = db.Column(db.String(255))
    idlpse = db.Column(db.Integer)
    kodesatker = db.Column(db.String(255))
