from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardLpseModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_lpse table"""
    __tablename__ = 'dashboard_lpse'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    namalpse = db.Column(db.String(255))
    website = db.Column(db.String(255))
