from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DlhOpendataIndeksLingkunganHidupJawaBaratModel(BaseModel, db.Model):
    """Model for the dlh_opendata_indeks_lingkungan_hidup_jawa_barat table"""
    __tablename__ = 'opendata_indeks_lingkungan_hidup_jawa_barat'
    __table_args__ = {'schema': 'dlh'}

    # start model
    indikator = db.Column(db.String(255))
    indeks = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
