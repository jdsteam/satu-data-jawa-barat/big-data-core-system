from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DtphHargaKomoditasModel(BaseModel, db.Model):
    """Model for the dtph_harga_komoditas table"""
    __tablename__ = 'harga_komoditas'
    __table_args__ = {'schema': 'dtph'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    kodewilayah = db.Column(db.String)
    namawilayah = db.Column(db.String)
    levelwilayah = db.Column(db.String)
    tahun = db.Column(db.Integer)
    bulan = db.Column(db.Integer)
    kodekomoditi = db.Column(db.Integer)
    avgharga = db.Column(db.Float)
    unitharga = db.Column(db.String)
    updatedtime = db.Column(db.DateTime(True))
