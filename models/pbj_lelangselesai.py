from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjLelangselesaiModel(BaseModel, db.Model):
    """Model for the pbj_lelangselesai table"""
    __tablename__ = 'lelangselesai'
    __table_args__ = {'schema': 'pbj'}

    # start model
    durasi = db.Column(db.String(255))
    id_rup = db.Column(db.String(255))
    jum_hps = db.Column(db.String(255))
    jum_pagu = db.Column(db.String(255))
    jum_penawar = db.Column(db.String(255))
    jum_peserta = db.Column(db.String(255))
    kategori_lelang = db.Column(db.String(255))
    kd_ang = db.Column(db.String(255))
    kd_lelang = db.Column(db.String(255))
    kd_lpse = db.Column(db.String(255))
    kd_mtd_pemilihan = db.Column(db.String(255))
    kd_paket = db.Column(db.String(255))
    kd_panitia = db.Column(db.String(255))
    kd_pemenang = db.Column(db.String(255))
    kd_satker = db.Column(db.String(255))
    nama_agency = db.Column(db.String(255))
    nama_paket = db.Column(db.String(255))
    nama_pemenang = db.Column(db.String(255))
    nilai_ang = db.Column(db.String(255))
    nilai_penawaran = db.Column(db.String(255))
    nilai_penawaran3 = db.Column(db.String(255))
    npwp_penyedia = db.Column(db.String(255))
    pagu_selesai = db.Column(db.String(255))
    paket_lokasi = db.Column(db.String(255))
    persen = db.Column(db.String(255))
    persen_paguhps = db.Column(db.String(255))
    sumber_dana = db.Column(db.String(255))
    tahun_anggaran = db.Column(db.String(255))
    tahun_anggaran_dce = db.Column(db.String(255))
    tanggal_mulai = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
