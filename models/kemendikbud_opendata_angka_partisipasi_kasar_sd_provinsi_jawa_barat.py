from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class KemendikbudOpendataAngkaPartisipasiKasarSdProvinsiJawaBaratModel(BaseModel, db.Model):
    """Model for the kemendikbud_opendata_angka_partisipasi_kasar_sd_provinsi_jawa_barat table"""
    __tablename__ = 'opendata_angka_partisipasi_kasar_sd_provinsi_jawa_barat'
    __table_args__ = {'schema': 'kemendikbud'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    apk_sd = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
