from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjPenyediaDanSwakelolaModel(BaseModel, db.Model):
    """Model for the pbj_penyedia_dan_swakelola table"""
    __tablename__ = 'penyedia_dan_swakelola'
    __table_args__ = {'schema': 'pbj'}

    # start model
    tanggal_terakhir_di_update = db.Column(db.String(255))
    kode_kldi = db.Column(db.String(255))
    id_satker = db.Column(db.Integer)
    kode_satker_asli = db.Column(db.String(255))
    jenis = db.Column(db.String(255))
    kldi = db.Column(db.String(255))
    kode_rup = db.Column(db.Integer)
    nama_satker = db.Column(db.String(255))
    nama_paket = db.Column(db.String(255))
    program = db.Column(db.String(255))
    kode_string_program = db.Column(db.String(255))
    kegiatan = db.Column(db.String(255))
    kode_string_kegiatan = db.Column(db.String(255))
    volume = db.Column(db.String(255))
    pagu_rup = db.Column(db.Integer)
    mak = db.Column(db.String(255))
    lokasi = db.Column(db.String(255))
    detail_lokasi = db.Column(db.String(255))
    sumber_dana = db.Column(db.String(255))
    metode_pemilihan = db.Column(db.String(255))
    jenis_pengadaan = db.Column(db.String(255))
    pagu_perjenis_pengadaan = db.Column(db.String(255))
    awal_pengadaan = db.Column(db.String(255))
    akhir_pengadaan = db.Column(db.String(255))
    awal_pekerjaan = db.Column(db.String(255))
    akhir_pekerjaan = db.Column(db.String(255))
    tanggal_kebutuhan = db.Column(db.String(255))
    spesifikasi = db.Column(db.String(255))
    id_swakelola = db.Column(db.Float)
    nama_kpa = db.Column(db.String(255))
    penyedia_didalam_swakelola = db.Column(db.String(255))
    tkdn = db.Column(db.String(255))
    pradipa = db.Column(db.String(255))
    status_aktif = db.Column(db.String(255))
    status_umumkan = db.Column(db.String(255))
    id_client = db.Column(db.String(255))
    nama_ppk = db.Column(db.String(255))
    nip_ppk = db.Column(db.String(255))
    nip_kpa = db.Column(db.String(255))
    deskripsi = db.Column(db.String(255))
    umkm = db.Column(db.String(255))
    label = db.Column(db.String(255))
    tipe_swakelola = db.Column(db.Float)
    kode_kldi_penyelenggara = db.Column(db.String(255))
    nama_kldi_penyelenggara = db.Column(db.String(255))
    kode_satker_penyelenggara = db.Column(db.Float)
    nama_satker_penyelenggara = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
