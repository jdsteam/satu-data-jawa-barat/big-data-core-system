from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DkppOpendataDaerahAsalPemasukanUnggasDanTelurKeProvinsiJawModel(BaseModel, db.Model):
    """Model for the dkpp_opendata_daerah_asal_pemasukan_unggas_dan_telur_ke_provinsi_jaw table"""
    __tablename__ = 'opendata_daerah_asal_pemasukan_unggas_dan_telur_ke_provinsi_jaw'
    __table_args__ = {'schema': 'dkpp'}

    # start model
    provinsi = db.Column(db.String(20))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(50))
    daerah_asal = db.Column(db.String(50))
    tahun = db.Column(db.Integer)
    id = db.Column(db.Integer, primary_key=True)
