from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BapendaPotensipajakJenispenerimaanModel(BaseModel, db.Model):
    """Model for the bapenda_potensipajak_jenispenerimaan table"""
    __tablename__ = 'potensipajak_jenispenerimaan'
    __table_args__ = {'schema': 'bapenda'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    nama = db.Column(db.String)
    koderekening = db.Column(db.String)
