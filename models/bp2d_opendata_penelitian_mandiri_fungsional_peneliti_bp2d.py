from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class Bp2DOpendataPenelitianMandiriFungsionalPenelitiBp2DModel(BaseModel, db.Model):
    """Model for the bp2d_opendata_penelitian_mandiri_fungsional_peneliti_bp2d table"""
    __tablename__ = 'opendata_penelitian_mandiri_fungsional_peneliti_bp2d'
    __table_args__ = {'schema': 'bp2d'}

    # start model
    judul_penelitian = db.Column(db.String(255))
    peneliti = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
