from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardUrusanModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_urusan table"""
    __tablename__ = 'dashboard_urusan'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    kodeurusan = db.Column(db.String(255))
    namaurusan = db.Column(db.String(255))
