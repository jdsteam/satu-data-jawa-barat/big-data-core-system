from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimGiatNogiatModel(BaseModel, db.Model):
    """Model for the pbj_dim_giat_nogiat table"""
    __tablename__ = 'dim_giat_nogiat'
    __table_args__ = {'schema': 'pbj'}

    # start model
    tahun = db.Column(db.Integer)
    idgiat = db.Column(db.Integer)
    kodegiat = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
