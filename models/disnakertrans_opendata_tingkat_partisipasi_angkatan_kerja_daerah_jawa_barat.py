from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisnakertransOpendataTingkatPartisipasiAngkatanKerjaDaerahJawaBaratModel(BaseModel, db.Model):
    """Model for the disnakertrans_opendata_tingkat_partisipasi_angkatan_kerja_daerah_jawa_barat table"""
    __tablename__ = 'opendata_tingkat_partisipasi_angkatan_kerja_daerah_jawa_barat'
    __table_args__ = {'schema': 'disnakertrans'}

    # start model
    daerah = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
