from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpkadOpendataJumlahPendapatanDanaPerimbanganJawaBaratModel(BaseModel, db.Model):
    """Model for the bpkad_opendata_jumlah_pendapatan_dana_perimbangan_jawa_barat table"""
    __tablename__ = 'opendata_jumlah_pendapatan_dana_perimbangan_jawa_barat'
    __table_args__ = {'schema': 'bpkad'}

    # start model
    pendapatan_daerah = db.Column(db.String(255))
    jenis_pendapatan = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
