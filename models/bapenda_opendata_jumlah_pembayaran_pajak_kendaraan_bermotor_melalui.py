from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BapendaOpendataJumlahPembayaranPajakKendaraanBermotorMelaluiModel(BaseModel, db.Model):
    """Model for the bapenda_opendata_jumlah_pembayaran_pajak_kendaraan_bermotor_melalui table"""
    __tablename__ = 'opendata_jumlah_pembayaran_pajak_kendaraan_bermotor_melalui'
    __table_args__ = {'schema': 'bapenda'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    kota_kabupaten = db.Column(db.String(255))
    cabang_pelayanan = db.Column(db.String(255))
    nama_bank = db.Column(db.String(255))
    pkb = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
