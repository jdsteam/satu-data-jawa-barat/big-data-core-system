from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisnakertransOpendataPendudukYangBekerjaParuhWaktuJenisPekerjaanModel(BaseModel, db.Model):
    """Model for the disnakertrans_opendata_penduduk_yang_bekerja_paruh_waktu_jenis_pekerjaan table"""
    __tablename__ = 'opendata_penduduk_yang_bekerja_paruh_waktu_jenis_pekerjaan'
    __table_args__ = {'schema': 'disnakertrans'}

    # start model
    jenis_pekerjaan_atau_jabatan = db.Column(db.String(255))
    penduduk_yang_bekerja_paruh_waktu = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
