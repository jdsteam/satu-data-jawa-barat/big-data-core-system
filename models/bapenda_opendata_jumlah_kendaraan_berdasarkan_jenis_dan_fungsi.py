from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BapendaOpendataJumlahKendaraanBerdasarkanJenisDanFungsiModel(BaseModel, db.Model):
    """Model for the bapenda_opendata_jumlah_kendaraan_berdasarkan_jenis_dan_fungsi table"""
    __tablename__ = 'opendata_jumlah_kendaraan_berdasarkan_jenis_dan_fungsi'
    __table_args__ = {'schema': 'bapenda'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    kota_kabupaten = db.Column(db.String(255))
    cabang_pelayanan = db.Column(db.String(255))
    jenis_kendaraan = db.Column(db.String(255))
    fungsi_kendaraan = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
