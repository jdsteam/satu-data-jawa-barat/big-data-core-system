from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjFactSpseModel(BaseModel, db.Model):
    """Model for the pbj_fact_spse table"""
    __tablename__ = 'fact_spse'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idlelang = db.Column(db.Float)
    idpaket = db.Column(db.Float)
    idlpse = db.Column(db.Float)
    idsatker = db.Column(db.Float)
    idmetodepemilihan = db.Column(db.Float)
    kodeanggaran = db.Column(db.String(255))
    idkategori = db.Column(db.Float)
    ket_ditutup = db.Column(db.String(255))
    ket_diulang = db.Column(db.String(255))
    versi = db.Column(db.Float)
    idpemenang = db.Column(db.Float)
    paketlokasi = db.Column(db.String(255))
    nilai_anggaran = db.Column(db.String(255))
    nilai_penawaran_1 = db.Column(db.Float)
    nilai_penawaran_2 = db.Column(db.Float)
    hps = db.Column(db.Float)
    pagu = db.Column(db.Float)
    id = db.Column(db.Integer, primary_key=True)
