from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpsPotensiWisataDesaModel(BaseModel, db.Model):
    """Model for the bps_potensi_wisata_desa table"""
    __tablename__ = 'potensi_wisata_desa'
    __table_args__ = {'schema': 'bps'}

    # start model
    nama_provinsi = db.Column(db.String(255))
    kode_provinsi = db.Column(db.String(255))
    nama_kabupaten_kota = db.Column(db.String(255))
    kode_kabupaten_kota = db.Column(db.String(255))
    nama_kecamatan = db.Column(db.String(255))
    kode_kecamatan = db.Column(db.String(255))
    nama_desa_kelurahan = db.Column(db.String(255))
    kode_desa_kelurahan = db.Column(db.String(255))
    pemanfaatan_laut_untuk__wisata_bahari = db.Column(db.String(255))
    penggunaan_sungai_untuk_pariwisata__komersial_ = db.Column(db.String(255))
    penggunaan_saluran_irigasi_untuk_pariwisata__komersial_ = db.Column(db.String(255))
    penggunaan_danau_waduk_situ_bendungan_untuk_pariwisata__komersi = db.Column(db.String(255))
    penggunaan_embung_untuk_pariwisata__komersial_ = db.Column(db.String(255))
    kegiatan__sarana_prasarana_rekreasi_dan_wisata = db.Column(db.String(255))
    sumber_dana__sarana_prasarana_rekreasi_dan_wisata = db.Column(db.String(255))
    pelaksana__sarana_prasarana_rekreasi_dan_wisata = db.Column(db.String(255))
    penerima_manfaat_langsung__kode__sarana_prasarana_rekreasi_dan_ = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
