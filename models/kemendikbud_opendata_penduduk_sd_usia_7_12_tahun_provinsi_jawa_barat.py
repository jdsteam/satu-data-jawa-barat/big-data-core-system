from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class KemendikbudOpendataPendudukSdUsia712TahunProvinsiJawaBaratModel(BaseModel, db.Model):
    """Model for the kemendikbud_opendata_penduduk_sd_usia_7_12_tahun_provinsi_jawa_barat table"""
    __tablename__ = 'opendata_penduduk_sd_usia_7_12_tahun_provinsi_jawa_barat'
    __table_args__ = {'schema': 'kemendikbud'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    penduduk_usia_7_12_tahun = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
