from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpkadOpendataJumlahPengeluaranPembiayaanDaerahJawaBaratModel(BaseModel, db.Model):
    """Model for the bpkad_opendata_jumlah_pengeluaran_pembiayaan_daerah_jawa_barat table"""
    __tablename__ = 'opendata_jumlah_pengeluaran_pembiayaan_daerah_jawa_barat'
    __table_args__ = {'schema': 'bpkad'}

    # start model
    pembiayaan_daerah = db.Column(db.String(255))
    jenis_pembiayaan = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
