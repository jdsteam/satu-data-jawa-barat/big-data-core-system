from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PsdaOpendataLuasDaerahIrigasiUtuhWsCiliwungCisadaneDanModel(BaseModel, db.Model):
    """Model for the psda_opendata_luas_daerah_irigasi_utuh_ws_ciliwung_cisadane_dan table"""
    __tablename__ = 'opendata_luas_daerah_irigasi_utuh_ws_ciliwung_cisadane_dan'
    __table_args__ = {'schema': 'psda'}

    # start model
    provinsi = db.Column(db.String(255))
    daerah_irigasi = db.Column(db.String(255))
    sumber_air = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    lokasi_kota_kabupaten = db.Column(db.String(255))
    luas_permen_pupr = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
