from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class MasterWilayahMergeKemendagri2017Bps2018KecamatanModel(BaseModel, db.Model):
    """Model for the master_wilayah_merge_kemendagri_2017_bps_2018_kecamatan table"""
    __tablename__ = 'wilayah_merge_kemendagri_2017_bps_2018_kecamatan'
    __table_args__ = {'schema': 'master'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    kemendagri_provinsi_kode = db.Column(db.String(255))
    kemendagri_kabupaten_kode = db.Column(db.String(255))
    kemendagri_kecamatan_kode = db.Column(db.String(255))
    kemendagri_provinsi_nama = db.Column(db.String(255))
    kemendagri_kabupaten_nama = db.Column(db.String(255))
    kemendagri_kecamatan_nama = db.Column(db.String(255))
    bps_provinsi_kode = db.Column(db.String(255))
    bps_kabupaten_kode = db.Column(db.String(255))
    bps_kecamatan_kode = db.Column(db.String(255))
    bps_provinsi_nama = db.Column(db.String(255))
    bps_kabupaten_nama = db.Column(db.String(255))
    bps_kecamatan_nama = db.Column(db.String(255))
    latitude = db.Column(db.String(255))
    longitude = db.Column(db.String(255))
    polygon_json = db.Column(db.String(255))
