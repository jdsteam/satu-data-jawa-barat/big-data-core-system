from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimPaketModel(BaseModel, db.Model):
    """Model for the pbj_dim_paket table"""
    __tablename__ = 'dim_paket'
    __table_args__ = {'schema': 'pbj'}

    # start model
    kd_paket = db.Column(db.Float)
    nama_paket = db.Column(db.String(255))
    jumlah_pagu = db.Column(db.Float)
    id = db.Column(db.Integer, primary_key=True)
