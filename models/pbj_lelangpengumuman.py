from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjLelangpengumumanModel(BaseModel, db.Model):
    """Model for the pbj_lelangpengumuman table"""
    __tablename__ = 'lelangpengumuman'
    __table_args__ = {'schema': 'pbj'}

    # start model
    nama_satker = db.Column(db.String(255))
    kd_satker = db.Column(db.String(255))
    kd_agency = db.Column(db.String(255))
    nama_agency = db.Column(db.String(255))
    nama_panitia = db.Column(db.String(255))
    kd_lpse = db.Column(db.String(255))
    nama_lpse = db.Column(db.String(255))
    prp_nama = db.Column(db.String(255))
    kd_rup = db.Column(db.String(255))
    kd_lelang = db.Column(db.String(255))
    kd_paket = db.Column(db.String(255))
    paket = db.Column(db.String(255))
    pagu = db.Column(db.String(255))
    hps = db.Column(db.String(255))
    kategori = db.Column(db.String(255))
    sumber_dana = db.Column(db.String(255))
    thn_anggaran = db.Column(db.String(255))
    tgl_pengumuman = db.Column(db.String(255))
    jadwaltanggal_kontrak = db.Column(db.String(255))
    kd_anggaran = db.Column(db.String(255))
    ket_diulang = db.Column(db.String(255))
    ket_ditutp = db.Column(db.String(255))
    versi = db.Column(db.String(255))
    lls_dibuat_tanggal = db.Column(db.String(255))
    lls_tgl_setuju = db.Column(db.String(255))
    stk_alamat = db.Column(db.String(255))
    stk_kode = db.Column(db.String(255))
    mtd_pemilihan = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
