from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisdukcapilOpendataJumlahKepemilikanAktaPerkawinanBerdasarkanJenisModel(BaseModel, db.Model):
    """Model for the disdukcapil_opendata_jumlah_kepemilikan_akta_perkawinan_berdasarkan_jenis table"""
    __tablename__ = 'opendata_jumlah_kepemilikan_akta_perkawinan_berdasarkan_jenis'
    __table_args__ = {'schema': 'disdukcapil'}

    # start model
    kode_kota_kabupaten = db.Column(db.Integer)
    kota_kabupaten = db.Column(db.String(255))
    jenis_kelamin = db.Column(db.String(255))
    jumlah_kepemilikan_akta_perkawinan = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
