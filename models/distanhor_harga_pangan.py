from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DistanhorHargaPanganModel(BaseModel, db.Model):
    """Model for the distanhor_harga_pangan table"""
    __tablename__ = 'harga_pangan'
    __table_args__ = {'schema': 'distanhor'}

    # start model
    gambar = db.Column(db.String(255))
    id = db.Column(db.String(255))
    nama = db.Column(db.String(255))
    rata_hari_ini = db.Column(db.String(255))
    rata_kemarin = db.Column(db.String(255))
    status_harga = db.Column(db.String(255))
    tanggal_entri = db.Column(db.String(255))
    i = db.Column(db.Integer, primary_key=True)
