from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskanlutOpendataLuasProduksiUsahaGaramProvinsiJawaBaratModel(BaseModel, db.Model):
    """Model for the diskanlut_opendata_luas_produksi_usaha_garam_provinsi_jawa_barat table"""
    __tablename__ = 'opendata_luas_produksi_usaha_garam_provinsi_jawa_barat'
    __table_args__ = {'schema': 'diskanlut'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    luas = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
