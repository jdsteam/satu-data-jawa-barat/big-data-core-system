from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PsdaOpendataPosDugaAirWsCitarumModel(BaseModel, db.Model):
    """Model for the psda_opendata_pos_duga_air_ws_citarum table"""
    __tablename__ = 'opendata_pos_duga_air_ws_citarum'
    __table_args__ = {'schema': 'psda'}

    # start model
    provinsi = db.Column(db.String(255))
    desa = db.Column(db.String(255))
    kecamatan = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    kota_kabupaten = db.Column(db.String(255))
    sungai = db.Column(db.String(255))
    das = db.Column(db.String(255))
    nama_pos = db.Column(db.String(255))
    nomor_pos = db.Column(db.String(255))
    luas = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
