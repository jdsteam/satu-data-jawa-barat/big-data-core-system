from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskesOpendataJumlahKematianBerdasarkanPenyebabProvinsiJawaModel(BaseModel, db.Model):
    """Model for the diskes_opendata_jumlah_kematian_berdasarkan_penyebab_provinsi_jawa table"""
    __tablename__ = 'opendata_jumlah_kematian_berdasarkan_penyebab_provinsi_jawa'
    __table_args__ = {'schema': 'diskes'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    kematian = db.Column(db.String(255))
    penyebab = db.Column(db.String(255))
    jumlah = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
