from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisbunOpendataProduksiTanamanSemusimPerkebunanNegaraTebuModel(BaseModel, db.Model):
    """Model for the disbun_opendata_produksi_tanaman_semusim_perkebunan_negara_tebu table"""
    __tablename__ = 'opendata_produksi_tanaman_semusim_perkebunan_negara_tebu'
    __table_args__ = {'schema': 'disbun'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kabupaten_kota = db.Column(db.Integer)
    nama_kabupaten_kota = db.Column(db.String(255))
    jumlah = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
