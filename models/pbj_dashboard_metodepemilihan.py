from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardMetodepemilihanModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_metodepemilihan table"""
    __tablename__ = 'dashboard_metodepemilihan'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    namametode = db.Column(db.String(255))
    description = db.Column(db.String(255))
