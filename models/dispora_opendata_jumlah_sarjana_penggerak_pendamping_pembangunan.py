from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisporaOpendataJumlahSarjanaPenggerakPendampingPembangunanModel(BaseModel, db.Model):
    """Model for the dispora_opendata_jumlah_sarjana_penggerak_pendamping_pembangunan table"""
    __tablename__ = 'opendata_jumlah_sarjana_penggerak_pendamping_pembangunan'
    __table_args__ = {'schema': 'dispora'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    jumlah = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
