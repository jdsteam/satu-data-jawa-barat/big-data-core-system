from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DtphDailyPriceModel(BaseModel, db.Model):
    """Model for the dtph_daily_price table"""
    __tablename__ = 'daily_price'
    __table_args__ = {'schema': 'dtph'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    kodekomoditi = db.Column(db.Integer)
    tanggal = db.Column(db.DateTime(True))
    kodewilayah = db.Column(db.String)
    namadesa = db.Column(db.String)
    namakecamatan = db.Column(db.String)
    namakabupaten = db.Column(db.String)
    harga = db.Column(db.Float)
