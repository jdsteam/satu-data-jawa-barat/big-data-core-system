from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PsdaOpendataPengelolaanSituJawaBaratModel(BaseModel, db.Model):
    """Model for the psda_opendata_pengelolaan_situ_jawa_barat table"""
    __tablename__ = 'opendata_pengelolaan_situ_jawa_barat'
    __table_args__ = {'schema': 'psda'}

    # start model
    wilayah_sungai = db.Column(db.String(255))
    kawasan = db.Column(db.String(255))
    pengelolaan = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
