from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class JdsOpendataKodeWilayahProvinsiModel(BaseModel, db.Model):
    """Model for the jds_opendata_kode_wilayah_provinsi table"""
    __tablename__ = 'opendata_kode_wilayah_provinsi'
    __table_args__ = {'schema': 'jds'}

    # start model
    id = db.Column(db.Integer)
    kemendagri_provinsi_kode = db.Column(db.Integer)
    kemendagri_provinsi_nama = db.Column(db.String(255))
    bps_provinsi_kode = db.Column(db.Integer)
    bps_provinsi_nama = db.Column(db.String(255))
    latitude = db.Column(db.String(255))
    longitude = db.Column(db.String(255))
    geojson = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id_serial = db.Column(db.Integer, primary_key=True)
