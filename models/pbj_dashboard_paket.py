from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardPaketModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_paket table"""
    __tablename__ = 'dashboard_paket'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idrup = db.Column(db.Integer)
    idlelang = db.Column(db.Integer)
    namapaket = db.Column(db.String(255))
    pagu = db.Column(db.Float)
    kodekegiatan = db.Column(db.String)
    idmetodepemilihan = db.Column(db.Integer)
    idkategori = db.Column(db.Integer)
    idtahapan = db.Column(db.Integer)
    idpenyedia = db.Column(db.Integer)
    idsatker = db.Column(db.Integer)
    tahun = db.Column(db.Integer)
    isselesai = db.Column(db.Boolean)
    start_date = db.Column(db.DateTime(True))
    end_date = db.Column(db.DateTime(True))
    updated_date = db.Column(db.DateTime(True))
    hps = db.Column(db.Float)
    pagu_selesai = db.Column(db.Float)
    isactive = db.Column(db.Boolean)
    isumumkan = db.Column(db.Boolean)
    id = db.Column(db.Integer, primary_key=True)
