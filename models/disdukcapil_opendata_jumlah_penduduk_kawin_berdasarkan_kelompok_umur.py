from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisdukcapilOpendataJumlahPendudukKawinBerdasarkanKelompokUmurModel(BaseModel, db.Model):
    """Model for the disdukcapil_opendata_jumlah_penduduk_kawin_berdasarkan_kelompok_umur table"""
    __tablename__ = 'opendata_jumlah_penduduk_kawin_berdasarkan_kelompok_umur'
    __table_args__ = {'schema': 'disdukcapil'}

    # start model
    umur = db.Column(db.String(255))
    jumlah_penduduk = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
