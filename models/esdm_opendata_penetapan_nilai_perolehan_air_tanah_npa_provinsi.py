from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class EsdmOpendataPenetapanNilaiPerolehanAirTanahNpaProvinsiModel(BaseModel, db.Model):
    """Model for the esdm_opendata_penetapan_nilai_perolehan_air_tanah_npa_provinsi table"""
    __tablename__ = 'opendata_penetapan_nilai_perolehan_air_tanah_npa_provinsi'
    __table_args__ = {'schema': 'esdm'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    kota_kabupaten = db.Column(db.String(255))
    bulan_pajak = db.Column(db.String(255))
    nilai_perolehan_air_tanah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
