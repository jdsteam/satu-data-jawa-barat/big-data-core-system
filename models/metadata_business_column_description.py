from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class MetadataBusinessColumnDescriptionModel(BaseModel, db.Model):
    """Model for the metadata_business_column_description table"""
    __tablename__ = 'business_column_description'
    __table_args__ = {'schema': 'metadata'}

    # start model
    table_schema = db.Column(db.String)
    table_name = db.Column(db.String)
    column_name = db.Column(db.String)
    column_type = db.Column(db.String)
    unit_of_measurement = db.Column(db.String(255))
    column_description = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
