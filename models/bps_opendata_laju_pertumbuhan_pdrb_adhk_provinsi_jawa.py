from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpsOpendataLajuPertumbuhanPdrbAdhkProvinsiJawaModel(BaseModel, db.Model):
    """Model for the bps_opendata_laju_pertumbuhan_pdrb_adhk_provinsi_jawa table"""
    __tablename__ = 'opendata_laju_pertumbuhan_pdrb_adhk_provinsi_jawa'
    __table_args__ = {'schema': 'bps'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    lpe = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
