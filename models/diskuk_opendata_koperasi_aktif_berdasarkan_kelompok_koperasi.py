from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskukOpendataKoperasiAktifBerdasarkanKelompokKoperasiModel(BaseModel, db.Model):
    """Model for the diskuk_opendata_koperasi_aktif_berdasarkan_kelompok_koperasi table"""
    __tablename__ = 'opendata_koperasi_aktif_berdasarkan_kelompok_koperasi'
    __table_args__ = {'schema': 'diskuk'}

    # start model
    kelompok_koperasi = db.Column(db.String(255))
    koperasi_aktif = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
