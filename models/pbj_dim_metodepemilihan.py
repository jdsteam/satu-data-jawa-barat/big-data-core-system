from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimMetodepemilihanModel(BaseModel, db.Model):
    """Model for the pbj_dim_metodepemilihan table"""
    __tablename__ = 'dim_metodepemilihan'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idmetodepemilihan = db.Column(db.Integer, primary_key=True)
    namametodepemilihan = db.Column(db.String(255))
