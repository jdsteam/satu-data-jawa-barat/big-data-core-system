from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisdukcapilOpendataJumlahKepalaKeluargaBerdasarkanKelompokModel(BaseModel, db.Model):
    """Model for the disdukcapil_opendata_jumlah_kepala_keluarga_berdasarkan_kelompok table"""
    __tablename__ = 'opendata_jumlah_kepala_keluarga_berdasarkan_kelompok'
    __table_args__ = {'schema': 'disdukcapil'}

    # start model
    kelompok_pekerjaan = db.Column(db.String(255))
    jenis_kelamin = db.Column(db.String(255))
    jumlah_pekerja = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
