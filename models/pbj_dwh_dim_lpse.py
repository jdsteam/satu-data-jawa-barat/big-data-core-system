from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDwhDimLpseModel(BaseModel, db.Model):
    """Model for the pbj_dwh_dim_lpse table"""
    __tablename__ = 'dwh_dim_lpse'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idlpse = db.Column(db.Float, primary_key=True)
    namalpse = db.Column(db.String(255))
