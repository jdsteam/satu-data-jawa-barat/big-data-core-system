from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisparbudOpendataJumlahAnggotaPenggerakPariwisataProvinsiJawaModel(BaseModel, db.Model):
    """Model for the disparbud_opendata_jumlah_anggota_penggerak_pariwisata_provinsi_jawa table"""
    __tablename__ = 'opendata_jumlah_anggota_penggerak_pariwisata_provinsi_jawa'
    __table_args__ = {'schema': 'disparbud'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    anggota = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
