from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpsPodes201832DesaKetModel(BaseModel, db.Model):
    """Model for the bps_podes2018_32_desa_ket table"""
    __tablename__ = 'podes2018_32_desa_ket'
    __table_args__ = {'schema': 'bps'}

    # start model
    No = db.Column(db.Integer)
    Nama_Variabel = db.Column(db.String(255))
    Kategori_Isian = db.Column(db.String(255))
    Keterangan = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
