from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDashboardKategoriModel(BaseModel, db.Model):
    """Model for the pbj_dashboard_kategori table"""
    __tablename__ = 'dashboard_kategori'
    __table_args__ = {'schema': 'pbj'}

    # start model
    id = db.Column(db.Integer, primary_key=True)
    namakategori = db.Column(db.String(255))
    description = db.Column(db.String(255))
