from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDwhDimKategoriModel(BaseModel, db.Model):
    """Model for the pbj_dwh_dim_kategori table"""
    __tablename__ = 'dwh_dim_kategori'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idkategori = db.Column(db.Integer, primary_key=True)
    kategori = db.Column(db.String(255))
