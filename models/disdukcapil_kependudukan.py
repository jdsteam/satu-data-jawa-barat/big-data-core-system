from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisdukcapilKependudukanModel(BaseModel, db.Model):
    """Model for the disdukcapil_kependudukan table"""
    __tablename__ = 'kependudukan'
    __table_args__ = {'schema': 'disdukcapil'}

    # start model
    nokabupaten = db.Column(db.String(255))
    kodekabupaten = db.Column(db.String(255))
    kodekabupaten1 = db.Column(db.String(255))
    namakabupaten = db.Column(db.String(255))
    nokecamatan = db.Column(db.String(255))
    kodekecamatan = db.Column(db.String(255))
    namakecamatan = db.Column(db.String(255))
    nokelurahan = db.Column(db.String(255))
    kodekelurahan = db.Column(db.String(255))
    namakelurahan = db.Column(db.String(255))
    norw = db.Column(db.Integer)
    nort = db.Column(db.Integer)
    jumlah_kk = db.Column(db.Integer)
    lakilaki = db.Column(db.Integer)
    perempuan = db.Column(db.Integer)
    total = db.Column(db.Integer)
    id = db.Column(db.Integer, primary_key=True)
