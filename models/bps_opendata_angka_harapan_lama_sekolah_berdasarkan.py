from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class BpsOpendataAngkaHarapanLamaSekolahBerdasarkanModel(BaseModel, db.Model):
    """Model for the bps_opendata_angka_harapan_lama_sekolah_berdasarkan table"""
    __tablename__ = 'opendata_angka_harapan_lama_sekolah_berdasarkan'
    __table_args__ = {'schema': 'bps'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    angka_harapan_lama_sekolah = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
