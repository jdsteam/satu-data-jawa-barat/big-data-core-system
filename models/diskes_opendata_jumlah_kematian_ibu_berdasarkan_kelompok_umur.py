from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DiskesOpendataJumlahKematianIbuBerdasarkanKelompokUmurModel(BaseModel, db.Model):
    """Model for the diskes_opendata_jumlah_kematian_ibu_berdasarkan_kelompok_umur table"""
    __tablename__ = 'opendata_jumlah_kematian_ibu_berdasarkan_kelompok_umur'
    __table_args__ = {'schema': 'diskes'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    kematian_ibu = db.Column(db.String(255))
    kelompok_umur = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
