from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimGiatBlModel(BaseModel, db.Model):
    """Model for the pbj_dim_giat_bl table"""
    __tablename__ = 'dim_giat_bl'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idgiat = db.Column(db.Integer)
    idbl = db.Column(db.Integer)
    id = db.Column(db.Integer, primary_key=True)
