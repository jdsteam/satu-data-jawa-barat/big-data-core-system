from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjSpseDimPaketModel(BaseModel, db.Model):
    """Model for the pbj_spse_dim_paket table"""
    __tablename__ = 'spse_dim_paket'
    __table_args__ = {'schema': 'pbj'}

    # start model
    kd_paket = db.Column(db.Float, primary_key=True)
    nama_paket = db.Column(db.String(255))
