from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DistanhorOpendataLuasTanamPadiMenurutKotaKabJawaBaratModel(BaseModel, db.Model):
    """Model for the distanhor_opendata_luas_tanam_padi_menurut_kota_kab_jawa_barat table"""
    __tablename__ = 'opendata_luas_tanam_padi_menurut_kota_kab_jawa_barat'
    __table_args__ = {'schema': 'distanhor'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    jenis_komoditi = db.Column(db.String(255))
    luas_tanam = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
