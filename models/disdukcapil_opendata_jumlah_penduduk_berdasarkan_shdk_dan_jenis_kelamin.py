from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisdukcapilOpendataJumlahPendudukBerdasarkanShdkDanJenisKelaminModel(BaseModel, db.Model):
    """Model for the disdukcapil_opendata_jumlah_penduduk_berdasarkan_shdk_dan_jenis_kelamin table"""
    __tablename__ = 'opendata_jumlah_penduduk_berdasarkan_shdk_dan_jenis_kelamin'
    __table_args__ = {'schema': 'disdukcapil'}

    # start model
    status_hubungan_dalam_keluarga = db.Column(db.String(255))
    laki_laki = db.Column(db.String(255))
    perempuan = db.Column(db.String(255))
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
