from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisnakertransOpendataAngkatanKerjaGolonganUmurModel(BaseModel, db.Model):
    """Model for the disnakertrans_opendata_angkatan_kerja_golongan_umur table"""
    __tablename__ = 'opendata_angkatan_kerja_golongan_umur'
    __table_args__ = {'schema': 'disnakertrans'}

    # start model
    golongan_umur = db.Column(db.String(255))
    angkatan_kerja = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
