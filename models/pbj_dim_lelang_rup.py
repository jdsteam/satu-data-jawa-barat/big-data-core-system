from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class PbjDimLelangRupModel(BaseModel, db.Model):
    """Model for the pbj_dim_lelang_rup table"""
    __tablename__ = 'dim_lelang_rup'
    __table_args__ = {'schema': 'pbj'}

    # start model
    idlelang = db.Column(db.Float)
    idrup = db.Column(db.Float)
    id = db.Column(db.Integer, primary_key=True)
