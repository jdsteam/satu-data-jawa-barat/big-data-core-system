from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisdukcapilOpendataRasioJenisKelaminBerdasarkanKotaKabProvinsiModel(BaseModel, db.Model):
    """Model for the disdukcapil_opendata_rasio_jenis_kelamin_berdasarkan_kota_kab_provinsi table"""
    __tablename__ = 'opendata_rasio_jenis_kelamin_berdasarkan_kota_kab_provinsi'
    __table_args__ = {'schema': 'disdukcapil'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    kota_kabupaten = db.Column(db.String(255))
    jenis_kelamin = db.Column(db.String(255))
    rasio_jenis_kelamin = db.Column(db.Integer)
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
