from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisdikOpendataJumlahUsahaKesehatanSekolahSmkKondisiProvinsiModel(BaseModel, db.Model):
    """Model for the disdik_opendata_jumlah_usaha_kesehatan_sekolah_smk_kondisi_provinsi table"""
    __tablename__ = 'opendata_jumlah_usaha_kesehatan_sekolah_smk_kondisi_provinsi'
    __table_args__ = {'schema': 'disdik'}

    # start model
    provinsi = db.Column(db.String(255))
    kode_kota_kabupaten = db.Column(db.Integer)
    nama_kota_kabupaten = db.Column(db.String(255))
    kondisi = db.Column(db.String(255))
    jumlah = db.Column(db.Integer)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    id = db.Column(db.Integer, primary_key=True)
