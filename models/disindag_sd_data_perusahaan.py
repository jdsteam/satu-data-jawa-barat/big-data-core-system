from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime

class DisindagSdDataPerusahaanModel(BaseModel, db.Model):
    """Model for the disindag_sd_data_perusahaan table"""
    __tablename__ = 'sd_data_perusahaan'
    __table_args__ = {'schema': 'disindag'}

    # start model
    no = db.Column(db.Integer, primary_key=True)
    detail_url = db.Column(db.String(300))
    tahun_input = db.Column(db.Integer)
    tahun_izin = db.Column(db.Integer)
    nama_industri = db.Column(db.String(300))
    penanggung_jawab = db.Column(db.String(300))
    jenis = db.Column(db.String(200))
    alamat = db.Column(db.String(500))
    kecamatan = db.Column(db.String(100))
    kabupaten_kota = db.Column(db.String(100))
