/*
 Navicat Premium Data Transfer

 Source Server         : PostgreSQL JDS Server 3
 Source Server Type    : PostgreSQL
 Source Server Version : 90611
 Source Host           : 103.122.5.84:5432
 Source Catalog        : bigdata
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90611
 File Encoding         : 65001

 Date: 26/11/2019 11:27:46
*/


-- ----------------------------
-- Sequence structure for generate_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."generate_id_seq";
CREATE SEQUENCE "public"."generate_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for template_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."template_id_seq";
CREATE SEQUENCE "public"."template_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for generate
-- ----------------------------
DROP TABLE IF EXISTS "public"."generate";
CREATE TABLE "public"."generate" (
  "endpoint" varchar(255) COLLATE "pg_catalog"."default",
  "opd" varchar(255) COLLATE "pg_catalog"."default",
  "table_name" varchar(255) COLLATE "pg_catalog"."default",
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "id" int4 NOT NULL DEFAULT nextval('generate_id_seq'::regclass)
)
;

-- ----------------------------
-- Records of generate
-- ----------------------------
INSERT INTO "public"."generate" VALUES ('template', 'public', 'template', NULL, 1);
INSERT INTO "public"."generate" VALUES ('generate', 'public', 'generate', NULL, 2);
INSERT INTO "public"."generate" VALUES ('pegawai_data', 'bkd', 'pegawai_data', NULL, 16);
INSERT INTO "public"."generate" VALUES ('bulan', 'bappenda', 'bulan', NULL, 17);
INSERT INTO "public"."generate" VALUES ('laporanpajak', 'bappenda', 'laporanpajak', NULL, 18);
INSERT INTO "public"."generate" VALUES ('laporanpajak_perlokasi', 'bappenda', 'laporanpajak_perlokasi', NULL, 19);
INSERT INTO "public"."generate" VALUES ('lokasi', 'bappenda', 'lokasi', NULL, 20);
INSERT INTO "public"."generate" VALUES ('potensipajak_jumlah', 'bappenda', 'potensipajak_jumlah', NULL, 21);
INSERT INTO "public"."generate" VALUES ('potensipajak_rinci', 'bappenda', 'potensipajak_rinci', NULL, 22);
INSERT INTO "public"."generate" VALUES ('potensipajak_umum', 'bappenda', 'potensipajak_umum', NULL, 23);

-- ----------------------------
-- Table structure for template
-- ----------------------------
DROP TABLE IF EXISTS "public"."template";
CREATE TABLE "public"."template" (
  "id" int4 NOT NULL DEFAULT nextval('template_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "notes" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of template
-- ----------------------------
INSERT INTO "public"."template" VALUES (1, 'a', 'a');
INSERT INTO "public"."template" VALUES (2, 'b', 'b');
INSERT INTO "public"."template" VALUES (5, 'c', 'c');
INSERT INTO "public"."template" VALUES (6, 'd', 'd');
INSERT INTO "public"."template" VALUES (7, 'e', 'e');
INSERT INTO "public"."template" VALUES (8, 'f', 'f');
INSERT INTO "public"."template" VALUES (9, 'g', 'g');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."generate_id_seq"
OWNED BY "public"."generate"."id";
SELECT setval('"public"."generate_id_seq"', 24, true);
ALTER SEQUENCE "public"."template_id_seq"
OWNED BY "public"."template"."id";
SELECT setval('"public"."template_id_seq"', 12, true);

-- ----------------------------
-- Primary Key structure for table template
-- ----------------------------
ALTER TABLE "public"."template" ADD CONSTRAINT "template_pkey" PRIMARY KEY ("id");
