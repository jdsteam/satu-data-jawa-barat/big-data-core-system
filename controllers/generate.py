from helpers import Helper
from datetime import datetime
from dateutil import parser
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from models import GenerateModel
from settings import configuration
from flask import request
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import distinct
import json
import os
import sqlalchemy as db_access
import shutil
import requests


config= configuration.Configuration()
helper = Helper()

# referensi query model sql alchemy
# https://docs.sqlalchemy.org/en/latest/orm/query.html


class GenerateController(object):

    def __init__(self, **kwargs):
        pass

    def get_all(self, where: dict, sort, limit, skip):

        try:
            # query postgresql
            result = db.session.query(GenerateModel)
            for attr, value in where.items():
                result = result.filter(getattr(GenerateModel, attr) == value)
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            generate = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                generate.append(temp)

            # check if empty
            generate = list(generate)
            if (len(generate) > 0):
                return True, generate
            else:
                return False, []

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})


    def get_count(self, where: dict):

        try:
            # query postgresql
            result = db.session.query(GenerateModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(GenerateModel, attr) == value)
            result = result.count()

            # change into dict
            generate = {}
            generate['count'] = result

            # check if empty
            if generate:
                return True, generate
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})


    def get_by_id(self, id):

        try:
            # execute database
            result = db.session.query(GenerateModel)
            result = result.get(id)

            if(result):
                result = result.__dict__
                result.pop('_sa_instance_state', None)
            else:
                result = {}

            # change into dict
            generate = result

            # check if empty
            if generate:
                return True, generate
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})


    def create(self, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json


            try:
                os.mkdir('static/doc/'+ json_send['opd'])
                os.mkdir('static/upload/'+ json_send['opd'])
                os.mkdir('static/download/'+ json_send['opd'])
            except OSError:
                print ("Creation of the directory %s failed" % json_send['opd'])
            else:
                print ("Successfully created the directory %s " % json_send['opd'])

            if os.path.exists("static/download/index.html"):
                src = os.path.realpath("static/download/index.html")
                shutil.copy(src, src.replace('download', 'download/'+json_send['opd']))
            else:
                print('no file')

            # check if data is exist on table
            stat, count = self.get_count(json_send)
            if stat:
                if count['count'] == 0:
                    # prepare data model
                    result = GenerateModel(**json_send)

                    # execute database
                    db.session.add(result)
                    db.session.commit()
                    result = result.to_dict()
                    res, generate = self.get_by_id(result['id'])
                else:
                    res = True
                    json_send['status'] = 'already exist, update api'
                    generate = json_send
            else:
                res = False
                generate = {}


            # generate json
            self.generate_json(json_send)

            # generate api
            self.generate_api(json_send)

            # generate doc
            self.generate_doc(json_send)

            # generate app_service
            self.generate_app_service(json_send)

            # check if exist
            if(res):
                return True, generate
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})


    def generate_app_service(self, json_data: dict):

        try:
            token = request.headers.get('Authorization', None)
            rolecheck_url = configuration.rolecheck_url

            # print('search app')
            # print(rolecheck_url+'app?search=/'+json_data['opd']+'/')
            req = requests.get(rolecheck_url+'app?search=/'+json_data['opd']+'/', headers={"Authorization":token, "Content-Type": "application/json"}, data=json.dumps({}))
            response = req.json()
            # print(response)

            if(response['error'] == 0):

                try:
                    # print('search app service')
                    # print(rolecheck_url+"app_service?where={'app_id': "+str(response['data'][0]['id'])+", 'controller':'"+json_data['endpoint']+"'}")
                    req1 = requests.get(rolecheck_url+"app_service?where={'app_id': "+str(response['data'][0]['id'])+", 'controller':'"+json_data['endpoint']+"'}", headers={"Authorization":token, "Content-Type": "application/json"}, data=json.dumps({}))
                    response1 = req1.json()
                    # print(response1)

                    if(response1['error'] == 1):

                        try:
                            payload2 = {}
                            payload2['app_id'] = str(response['data'][0]['id'])
                            payload2['controller'] = json_data['endpoint']
                            payload2['action'] = 'GET'
                            payload2['dataset_class_id'] = '4'
                            payload2['notes'] = ''
                            payload2['is_active'] = True
                            payload2['is_deleted'] = False
                            payload2['is_backend'] = False
                            # print('insert app_service')
                            # print(payload2)
                            req2 = requests.post(rolecheck_url+'app_service', headers={"Authorization":token, "Content-Type": "application/json"}, data=json.dumps(payload2))
                            response2 = req2.json()
                            # print(response2)

                            if(response2['error'] == 0):

                                try:
                                    payload3 = {}
                                    payload3['user_id'] = '1'
                                    payload3['app_id'] = str(response2['data']['app']['id'])
                                    payload3['app_service_id'] = str(response2['data']['id'])
                                    payload3['enable'] = True
                                    payload3['notes'] = ''
                                    # print('insert user permission')
                                    # print(payload3)
                                    req3 = requests.post(rolecheck_url+'user_permission', headers={"Authorization":token, "Content-Type": "application/json"}, data=json.dumps(payload3))
                                    response3 = req3.json()
                                    # print(response3)

                                except Exception as e:
                                    # fail response
                                    # print("insert user permission " + str(e))
                                    raise ErrorMessage("get app service " + str(e), 500, 1, {})

                        except Exception as e:
                            # fail response
                            # print("insert app service " + str(e))
                            raise ErrorMessage("get app service " + str(e), 500, 1, {})

                except Exception as e:
                    # fail response
                    # print("get app service " + str(e))
                    raise ErrorMessage("get app service " + str(e), 500, 1, {})

        except Exception as e:
            # fail response
            # print("get app " + str(e))
            raise ErrorMessage("get app " + str(e), 500, 1, {})

    def generate_json(self, json_data: dict):

        try:

            table_name = json_data['table_name']
            endpoint = json_data['endpoint']
            opd = json_data['opd']

            try:
                engine = db_access.create_engine(config.SQLALCHEMY_DATABASE_URI)
                connection = engine.connect()
                metadata = db_access.MetaData()
                census = db_access.Table(table_name, metadata, autoload=True, autoload_with=engine, schema=opd)
            except Exception as e:
                print(e)

            # print(metadata.tables)
            string = repr(metadata.tables[opd+'.'+table_name])[6:-1]
            array1 = string.split(', ')
            # print(string)
            # print(len(array1))

            array2 = ""
            i = 0
            for arr in array1:
                i = i + 1
                if(i > 1 and i < len(array1)):
                    array2 = array2 + ', ' + arr
            # print(array2)


            array3 = array2.replace(', MetaData(bind=None), ', '')
            array3 = array3.split(', Column')

            array4 = []
            for arr in array3:
                arr = arr.replace('Column','')
                array4.append(arr[1:-1])
            # print(array4)

            model = []
            for arr in array4:
                # print(arr)
                arr = arr.split(', ')
                temp = {}
                temp['column'] = arr[0].replace("'", "")
                temp['data_type'] = arr[1]
                temp['data_type'] = temp['data_type'].replace('length=', '')
                temp['data_type'] = temp['data_type'].replace('TEXT()', 'String(255)').replace('VARCHAR', 'String').replace('CHAR', 'String')
                temp['data_type'] = temp['data_type'].replace('INTEGER', 'Integer').replace('BIGINT', 'Integer').replace('SMALLINT', 'Integer')
                temp['data_type'] = temp['data_type'].replace('BOOLEAN', 'Boolean').replace('DATE', 'DateTime(True)').replace('JSONB(astext_type=Text)', 'JSON')
                if("DOUBLE_PRECISION" in temp['data_type']):
                    temp['data_type'] = 'Float'
                if("TIMESTAMP" in temp['data_type']):
                    temp['data_type'] = 'DateTime(True)'
                temp['data_type'] = temp['data_type'].replace('()', '')
                temp['is_primary'] = False

                for a in arr:
                    is_primary = "primary_key" in a
                    if(is_primary):
                        temp['is_primary'] = True
                model.append(temp)

            result = {}
            result["table_name"] = table_name
            result["endpoint"] = endpoint
            result["opd"] = opd
            result["model"] = model
            # print(json.dumps(result))

            with open("static/upload/"+opd+'/'+endpoint+".json", "wt") as f:
                json.dump(result, f, sort_keys=True, indent=4)

        except Exception as e:
            # fail response
            raise ErrorMessage("Err generate json " + str(e), 500, 1, {})


    def generate_api(self, json_data: dict):

        try:

            table_name_ = json_data['table_name']
            endpoint_ = json_data['endpoint']
            opd_ = json_data['opd']

            with open('static/upload/'+opd_+'/'+endpoint_+'.json') as f:
                data_json = json.load(f)

            # value
            table_name = data_json['table_name']
            endpoint = data_json['endpoint']
            opd = data_json['opd']
            model = data_json['model']
            # print(table_name)
            # print(endpoint)
            # print(opd)

            # name generator
            file_name = opd.lower() + '_' + endpoint.replace(' ', '_').replace('-', '_').lower()
            controller_name = opd.title()+endpoint.title().replace(' ', '').replace('_', '').replace('-', '')+'Controller'
            model_name = opd.title()+endpoint.title().replace(' ', '').replace('_', '').replace('-', '')+'Model'

            # duplicate controller
            self.replace_code('controllers/template.py', 'controllers/' + file_name + '.py', file_name, opd, table_name, controller_name, model_name)

            # read file init controller
            input_from, input_controller = self.read_line_register('controllers/__init__.py')

            # add code to file init controller
            self.replace_line_register('controllers/__init__.py', input_from, 'from .'+file_name+' import '+controller_name+'\n\n')
            self.replace_line_register('controllers/__init__.py', input_controller+1, '\t"'+controller_name+'",\n]\n')


            # duplicate model
            self.replace_code('models/template.py', 'models/' + file_name + '.py', file_name, opd, table_name, controller_name, model_name)

            # register model
            input_model = self.read_line_model('models/' + file_name + '.py')

            # add code to model
            self.replace_line_model('models/' + file_name + '.py', input_model+1, '')
            for mdl in model:
                if(mdl['is_primary']):
                    text = "    " + mdl['column'] + " = db.Column(db." + mdl['data_type'] + ", primary_key=True)"
                else:
                    text = "    " + mdl['column'] + " = db.Column(db." + mdl['data_type'] + ")"
                with open('models/' + file_name + '.py', 'a') as fd:
                    fd.write(text+'\n')

            # read file init model
            input_from, input_model = self.read_line_register('models/__init__.py')

            # add code to file init controller
            self.replace_line_register('models/__init__.py', input_from, 'from .'+file_name+' import '+model_name+'\n\n')
            self.replace_line_register('models/__init__.py', input_model+1, '\t"'+model_name+'",\n]\n')


            # duplicate route
            self.replace_code('routes/template.py', 'routes/' + file_name + '.py', endpoint.replace(' ', '_').replace('-', '_').lower(), opd, table_name, controller_name, model_name)

            # read file init route
            input_from, input_route = self.read_line_register('routes/__init__.py')

            # add code to file init controller
            self.replace_line_register('routes/__init__.py', input_from, 'from . import '+file_name+'\n\n')
            self.replace_line_register('routes/__init__.py', input_route+1, '\t'+file_name+',\n]\n')


            # register blueprint
            input_blueprint = self.read_line_blueprint('settings/http.py')

            # add code to blueprint
            self.replace_line_blueprint('settings/http.py', input_blueprint, "    app.register_blueprint(routes."+file_name+".bp, url_prefix='/"+opd+"')\n    # end of blueprint\n")


        except Exception as e:
            # fail response
            raise ErrorMessage("Err generate api " + str(e), 500, 1, {})


    def generate_doc(self, json_data:dict):

        try:

            table_name = json_data['table_name']
            endpoint = json_data['endpoint']
            opd = json_data['opd']

            file_name = endpoint.replace(' ', '_').replace('-', '_')
            controller_name = endpoint.title().replace(' ', '').replace('_', '').replace('-', '')

            json_file = 'static/upload/'+opd+'/'+endpoint+'.json'
            old_file = 'static/doc/template.json'
            new_file = 'static/doc/' + opd + '/' + endpoint + '.json'

            with open(json_file, "rt", encoding='UTF8') as f1:
                model = f1.read()

                with open(old_file, "rt", encoding='UTF8') as f2:
                    newText = f2.read()
                    newText = newText.replace('template', file_name)
                    newText = newText.replace('Template', controller_name)

                    loaded_json = json.loads(newText)
                    # print(loaded_json['components']['schemas']['Model'+controller_name+'Body']['properties'])
                    # print(loaded_json['components']['schemas']['Model'+controller_name+'Response']['properties'])

                    loaded_json['components']['schemas']['Model'+controller_name+'Body']['properties'] = {}
                    loaded_json['components']['schemas']['Model'+controller_name+'Response']['properties'] = {}
                    for md in json.loads(model)['model']:
                        column = str(md['column'])
                        data_type = md['data_type'].split('(')

                        # print(isinstance(data_type, list))
                        if isinstance(data_type, list):
                            data_type = data_type[0].lower()
                        else:
                            data_type = md['data_type'].lower()

                        # print(column)
                        # print(data_type)

                        if(column != 'id'):
                            loaded_json['components']['schemas']['Model'+controller_name+'Body']['properties'][column] = {}
                            loaded_json['components']['schemas']['Model'+controller_name+'Body']['properties'][column]['type'] = data_type

                        loaded_json['components']['schemas']['Model'+controller_name+'Response']['properties'][column] = {}
                        loaded_json['components']['schemas']['Model'+controller_name+'Response']['properties'][column]['type'] = data_type


                    loaded_json['servers'][0]['url'] = '/' + opd + '/'
                    # print(loaded_json['components']['schemas']['Model'+controller_name+'Body']['properties'])
                    # print(loaded_json['components']['schemas']['Model'+controller_name+'Response']['properties'])

                with open(new_file, "wt") as f2:
                    json.dump(loaded_json, f2, sort_keys=True, indent=4)

        except Exception as e:
            # fail response
            raise ErrorMessage("Err generate doc " + str(e), 500, 1, {})


    def read_line_register(self, file_name):
        f = open(file_name, "r")
        result = f.read().split("\n")

        input_from = 0
        input_controller = 0
        i = 0

        for res in result:
            if(res == ''):
                if(input_from == 0):
                    input_from = i
            if(res == ']'):
                if(input_controller == 0):
                    input_controller = i

            i = i+1
        return input_from, input_controller


    def replace_line_register(self, file_name, line_num, text):
        lines = open(file_name, 'r').readlines()
        lines[line_num] = text
        out = open(file_name, 'w')
        out.writelines(lines)
        out.close()


    def read_line_blueprint(self, file_name):
        f = open(file_name, "r")
        result = f.read().split("\n")

        input = 0
        i = 0

        for res in result:
            if(res == '    # end of blueprint'):
                if(input == 0):
                    input = i
            i = i + 1
        return input


    def replace_line_blueprint(self, file_name, line_num, text):
        lines = open(file_name, 'r').readlines()
        lines[line_num] = text
        out = open(file_name, 'w')
        out.writelines(lines)
        out.close()

    def read_line_model(self, file_name):
        f = open(file_name, "r")
        result = f.read().split("\n")

        input = 0
        i = 0

        for res in result:
            if(res == '    # start model'):
                if(input == 0):
                    input = i
            i = i + 1
        return input


    def replace_line_model(self, file_name, line_num, text):
        lines = open(file_name, 'r').readlines()
        lines[line_num] = text
        out = open(file_name, 'w')
        out.writelines(lines)
        out.close()



    def replace_code(self, old_file, new_file, file_name, opd, table_name, controller_name, model_name):
        # print(old_file, new_file, file_name, opd, table_name, controller_name, model_name)
        with open(old_file, "rt") as f1:
            newText = f1.read()
            newText = newText.replace('TemplateController', controller_name)
            newText = newText.replace('TemplateModel', model_name)
            newText = newText.replace("__tablename__ = 'template'", "__tablename__ = '"+table_name+"'")
            newText = newText.replace("__table_args__ = {'schema': 'public'}", "__table_args__ = {'schema': '"+opd+"'}")
            newText = newText.replace('template', file_name)
            newText = newText.replace("render_"+file_name, "render_template")
            newText = newText.replace("/static/doc", "/static/doc/"+opd)
            newText = newText.replace("static/upload", "static/upload/"+opd)
            newText = newText.replace("static/download", "static/download/"+opd)


        with open(new_file, "wt") as f2:
            f2.write(newText)
