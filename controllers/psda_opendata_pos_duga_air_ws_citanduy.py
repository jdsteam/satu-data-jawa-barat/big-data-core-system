from settings import configuration
from helpers import Helper
from dateutil import parser
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from models import PsdaOpendataPosDugaAirWsCitanduyModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import distinct
from sqlalchemy import or_
from sqlalchemy import join
from sqlalchemy import func
from sqlalchemy import cast
import sqlalchemy
from flask import request
import json 
import datetime
import requests
import time


helper = Helper()

# referensi query model sql alchemy
# https://docs.sqlalchemy.org/en/latest/orm/query.html


class PsdaOpendataPosDugaAirWsCitanduyController(object):

    def __init__(self, **kwargs):
        pass

    def get_all(self, where: dict, search, sort, limit, skip, exclude):

        try:
            # query postgresql
            result = db.session.query(PsdaOpendataPosDugaAirWsCitanduyModel)
            if(len(exclude) > 0):
                for ex in exclude:
                    result = result.options(defer(ex))
            for attr, value in where.items():
                result = result.filter(getattr(PsdaOpendataPosDugaAirWsCitanduyModel, attr) == value)
            result = result.filter(or_(cast(getattr(PsdaOpendataPosDugaAirWsCitanduyModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in PsdaOpendataPosDugaAirWsCitanduyModel.__table__.columns ))
            if(sort[0] != 'null'):
                result = result.order_by(text(sort[0]+" "+sort[1]))
            if(limit != 0):
                result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            psda_opendata_pos_duga_air_ws_citanduy = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                for key in temp: 
                    if(isinstance(temp[key], datetime.datetime)):
                        temp[key] = temp[key].strftime('%Y-%m-%d %H:%M:%S')
                    elif(isinstance(temp[key], datetime.date)):
                        temp[key] = temp[key].strftime('%Y-%m-%d')
                    try:
                        temp[key] = json.loads(temp[key])  
                    except Exception as e: 
                        pass
                        # print(e)
                        
                psda_opendata_pos_duga_air_ws_citanduy.append(temp)

            # check if empty
            psda_opendata_pos_duga_air_ws_citanduy = list(psda_opendata_pos_duga_air_ws_citanduy)
            if (len(psda_opendata_pos_duga_air_ws_citanduy) > 0):
                return True, psda_opendata_pos_duga_air_ws_citanduy
            else:
                return False, []

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_count(self, where: dict, search):

        try:
            # query postgresql
            result = db.session.query(PsdaOpendataPosDugaAirWsCitanduyModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(PsdaOpendataPosDugaAirWsCitanduyModel, attr) == value)
            result = result.filter(or_(cast(getattr(PsdaOpendataPosDugaAirWsCitanduyModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in PsdaOpendataPosDugaAirWsCitanduyModel.__table__.columns ))
            result = result.count()

            # change into dict
            psda_opendata_pos_duga_air_ws_citanduy = {}
            psda_opendata_pos_duga_air_ws_citanduy['count'] = result

            # check if empty
            if psda_opendata_pos_duga_air_ws_citanduy:
                return True, psda_opendata_pos_duga_air_ws_citanduy
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_by_id(self, id, exclude):

        try:
            # execute database
            result = db.session.query(PsdaOpendataPosDugaAirWsCitanduyModel)
            if(len(exclude) > 0):
                for ex in exclude:
                    result = result.options(defer(ex))
            result = result.get(id)

            if(result):
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                for key in result: 
                    if(isinstance(result[key], datetime.datetime)):
                        result[key] = result[key].strftime('%Y-%m-%d %H:%M:%S')
                    elif(isinstance(result[key], datetime.date)):
                        result[key] = result[key].strftime('%Y-%m-%d')
                    try:
                        result[key] = json.loads(result[key])  
                    except Exception as e:
                        pass
                        # print(e)
            else:
                result = {}

            # change into dict
            psda_opendata_pos_duga_air_ws_citanduy = result

            # check if empty
            if psda_opendata_pos_duga_air_ws_citanduy:
                return True, psda_opendata_pos_duga_air_ws_citanduy
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def create(self, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json 

            # prepare data model
            result = PsdaOpendataPosDugaAirWsCitanduyModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, psda_opendata_pos_duga_air_ws_citanduy = self.get_by_id(result['id'], "")

            # check if exist
            if(res):
                return True, psda_opendata_pos_duga_air_ws_citanduy
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def update(self, where: dict, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json 

            try:
                # prepare data model
                result = db.session.query(PsdaOpendataPosDugaAirWsCitanduyModel)
                for attr, value in where.items():
                    result = result.filter(getattr(PsdaOpendataPosDugaAirWsCitanduyModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, psda_opendata_pos_duga_air_ws_citanduy = self.get_by_id(where["id"], "")

                # check if empty
                if (res):
                    return True, psda_opendata_pos_duga_air_ws_citanduy
                else:
                    return False, {}

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def delete(self, where: dict):

        try:
            # query
            where = where

            try:
                # prepare data model
                result = db.session.query(PsdaOpendataPosDugaAirWsCitanduyModel)
                for attr, value in where.items():
                    result = result.filter(getattr(PsdaOpendataPosDugaAirWsCitanduyModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, psda_opendata_pos_duga_air_ws_citanduy = self.get_by_id(where["id"], "")

                # check if exist
                if(res):
                    return False
                else:
                    return True

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def thead_count(self, dataset_id, token, jwt, satudata_url):

        url_dataset = satudata_url + 'dataset/' + str(dataset_id)
        url_history = satudata_url + 'history'
        # url_review = satudata_url + 'review'
        # url_notification = satudata_url + 'notification'

        # count access 
        # print('count access')
        try:
            req = requests.get(url_dataset, headers={"Authorization":token})
            res = req.json()

            if(res):
                count_access = res['data']['count_access']
                name = res['data']['name']
                if(count_access == None):
                    count_access = '0'
                count_access = int(count_access)+1

                # update data count
                # print('edit count')
                try:
                    jwt = helper.decode_jwt(token)
                    if(res['data']['kode_skpd'] != jwt['kode_skpd']):
                        payload = {}
                        payload['count_access'] = count_access
                        req = requests.put(url_dataset, headers={"Authorization":token, "Content-Type": "application/json"}, data=json.dumps(payload))
                        res = req.json()
                except Exception as e:
                    print(e)
                    
                # post history
                # print('tambah history')
                try:
                    payload = {}
                    payload['type'] = 'dataset'
                    payload['type_id'] = dataset_id
                    payload['user_id'] = jwt['id']
                    payload['category'] = 'access'
                    req = requests.post(url_history, headers={"Authorization":token, "Content-Type": "application/json"}, data=json.dumps(payload))
                    res = req.json()
                except Exception as e:
                    print(e)

                # # check review
                # # print('check review')
                # try:
                #     param = {}
                #     param['type'] = 'dataset'
                #     param['type_id'] = dataset_id
                #     param['user_id'] = jwt['id']
                #     req = requests.get(url_review + "?where="+str(param)+"&count=true", headers={"Authorization":token, "Content-Type": "application/json"})
                #     res = req.json()
                    
                #     if(res['data']['count'] < 1):
                        
                #         # post notification
                #         # print('send notif')
                #         try:
                #             payload = {}
                #             payload['type'] = 'dataset'
                #             payload['type_id'] = dataset_id
                #             payload['receiver'] = jwt['id']
                #             payload['title'] = name
                #             payload['content'] = 'menunggu untuk Anda beri ulasan.'
                #             payload['is_read'] = False
                #             req = requests.post(url_notification, headers={"Authorization":token, "Content-Type": "application/json"}, data=json.dumps(payload))
                #             res = req.json()
                #         except Exception as e:
                #             print(e)

                #     else:
                #         pass
                #         # print('tidak send notif')

                # except Exception as e:
                #     print(e)

            else:
                pass
                # print('tidak dapat data')

        except Exception as e:
            print(e)
