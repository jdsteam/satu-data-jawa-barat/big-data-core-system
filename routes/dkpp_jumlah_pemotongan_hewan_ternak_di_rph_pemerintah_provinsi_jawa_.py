import json
import csv
import sys
import threading

from flask import Blueprint
from flask import request
from flask import Response
from flask import render_template
from flask import send_file

from helpers import Helper
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from settings import configuration
from controllers import DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController

bp = Blueprint(__name__, 'jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_')
DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController = DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController()
helper = Helper()


@bp.route("/jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_", methods=["GET"])
@jwt_check()
def get_all():

    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", '')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        download = request.args.get("download", '')
        exclude = request.args.get("exclude", '')
        dataset_id = request.args.get("dataset_id", '0')

        # prepare data filter
        if(len(str(sort)) > 0):
            sort = sort.split(':')
        else:
            sort = []
            sort.append('null')

        if(len(str(exclude)) > 0):
            exclude = exclude.split(',')

        where = where.replace("'", "\"")
        where = json.loads(where)

        with open('static/upload/dkpp/jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_.json') as f:
            data_json = json.load(f) 

        metadata = []
        for dj in data_json['model']: 
            metadata.append(dj['column']) 

        if(count):        
            # call function get
            res, jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_ = DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.get_count(where, search)
        elif(('csv' in download.lower()) or ('pdf' in download.lower()) or ('xls' in download.lower())):
            # call function download
            if(download.lower() == 'csv'):
                res, jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_ = DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.get_all(where, search, sort, 0, 0, exclude) 
                filename = helper.convert_csv('static/download/dkpp/jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_.csv', jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_, metadata)
                # counter access & send notif
                if(dataset_id != '0'):
                    jwt = helper.read_jwt()
                    token = request.headers.get('Authorization', None)
                    satudata_url = configuration.satudata_url
                    thread0 = threading.Thread(target=DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.thead_count, args=(dataset_id, token, jwt, satudata_url, ))
                    thread0.start()
                return send_file('../'+filename, mimetype='application/x-csv', attachment_filename='jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_.csv', as_attachment=True)
            elif(download.lower() == 'xls'): 
                res, jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_ = DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.get_all(where, search, sort, 0, 0, exclude) 
                filename = helper.convert_xls('static/download/dkpp/jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_.xlsx', jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_, metadata) 
                # counter access & send notif
                if(dataset_id != '0'):
                    jwt = helper.read_jwt()
                    token = request.headers.get('Authorization', None)
                    satudata_url = configuration.satudata_url
                    thread0 = threading.Thread(target=DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.thead_count, args=(dataset_id, token, jwt, satudata_url, ))
                    thread0.start()
                return send_file('../'+filename, mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', attachment_filename='jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_.xlsx', as_attachment=True)
            elif(download.lower() == 'pdf'):
                res, jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_ = DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.get_all(where, search, sort, 0, 0, exclude) 
                filename = helper.convert_csv('static/download/dkpp/jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_.csv', jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_, metadata)
                filename2 = helper.convert_pdf(filename)
                # counter access & send notif
                if(dataset_id != '0'):
                    jwt = helper.read_jwt()
                    token = request.headers.get('Authorization', None)
                    satudata_url = configuration.satudata_url
                    thread0 = threading.Thread(target=DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.thead_count, args=(dataset_id, token, jwt, satudata_url, ))
                    thread0.start()
                return send_file('../'+filename2, mimetype='application/pdf', attachment_filename='jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_.pdf', as_attachment=True)
        else:
            # call function get
            res, jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_ = DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.get_all(where, search, sort, limit, skip, exclude)

        # response
        if(res):
            # success response
            response = {"message": "Get data successfull",
                        "error": 0, "data": jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_, "metadata": metadata}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found",
                        "error": 1, "data": [], "metadata": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": [], "metadata": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_/<id>", methods=["GET"])
@jwt_check()
def get_by_id(id):

    try:
        # filter data
        exclude = request.args.get("exclude", '')
 
        # prepare filter data
        if(len(str(exclude)) > 0):
            exclude = exclude.split(',')

        # call function get by id
        res, jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_ = DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.get_by_id(id, exclude)

        # response
        if(res):
            # success response
            response = {"message": "Get detail data successfull",
                        "error": 0, "data": jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_", methods=["POST"])
@jwt_check()
def create():

    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_
        res, jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_ = DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.create(json_request)

        # response
        if(res):
            # success response
            response = {"message": "Create data successfull",
                        "error": 0, "data": jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, ",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_/<id>", methods=["PUT"])
@jwt_check()
def update(id):

    try:
        # filter data
        where = {'id': id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_
        res, jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_ = DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.update(where, json_request)

        # response
        if(res):
            # success response
            response = {"message": "Update data successfull",
                        "error": 0, "data": jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Update data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_/<id>", methods=["DELETE"])
@jwt_check()
def delete(id):

    try:
        # filter data
        where = {'id': id}

        # delete jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_
        res = DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.delete(where)

        # response
        if(res):
            # success response
            response = {"message": "Delete data successfull",
                        "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_/doc", methods=["GET"])
def documentation():

    dataset_id = request.args.get("dataset_id", '0')

    if(dataset_id != '0'):
        jwt = helper.read_jwt()
        token = request.headers.get('Authorization', None)
        satudata_url = configuration.satudata_url
        thread0 = threading.Thread(target=DkppJumlahPemotonganHewanTernakDiRphPemerintahProvinsiJawaController.thead_count, args=(dataset_id, token, jwt, satudata_url, ))
        thread0.start()

    default_config = {
        'app_name': 'Template',
        'dom_id': '#swagger-ui',
        'url': '/static/doc/dkpp/jumlah_pemotongan_hewan_ternak_di_rph_pemerintah_provinsi_jawa_.json',
        'layout': 'StandaloneLayout'
    }

    fields = {
        'base_url': '/static',
        'app_name': default_config.pop('app_name'),
        'config_json': json.dumps(default_config),

    }  

    return render_template('swagger.html', **fields)