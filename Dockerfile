FROM python:3.5-slim
COPY . /app
WORKDIR /app
RUN apt-get update -qy && \
    apt-get install -qq -y \
    build-essential libpq-dev && \
    apt-get clean && \
    pip install --upgrade pip && \
    pip install -r ./requirements.txt
EXPOSE 5003
CMD python ./run.py